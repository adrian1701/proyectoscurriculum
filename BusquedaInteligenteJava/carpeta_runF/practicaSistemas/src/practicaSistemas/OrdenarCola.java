
package practicaSistemas;

import java.util.Comparator;


public class OrdenarCola implements Comparator<Nodo> {
	 
	public int compare(Nodo n1, Nodo n2) {
		if (n1.getValoracion() > n2.getValoracion()) return 1;
		else if (n1.getValoracion() < n2.getValoracion()) return -1;
		else return 0;
	}
}
