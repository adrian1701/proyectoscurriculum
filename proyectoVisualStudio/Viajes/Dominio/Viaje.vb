﻿Public Class Viaje
    Private fechaViaje As Date
    Private _tren As Tren
    Private _Productos, _toneladas As Collection
    Private mGestorViaje As GestorViaje
    Property Fecha As Date
        Get
            Return fechaViaje
        End Get
        Set(ByVal value As Date)
            fechaViaje = value
        End Set
    End Property
    Property Tren As Tren
        Get
            Return _tren
        End Get
        Set(ByVal value As Tren)
            _tren = value
        End Set
    End Property
    Property Productos As Collection
        Get
            Return _Productos
        End Get
        Set(value As Collection)
            _Productos = value
        End Set
    End Property
    Property Toneladas As Collection
        Get
            Return _toneladas
        End Get
        Set(value As Collection)
            _toneladas = value
        End Set
    End Property
    Property Gestor As GestorViaje
        Get
            Return mGestorViaje
        End Get
        Set(value As GestorViaje)

        End Set
    End Property

    Sub New()
        _tren = New Tren
        _Productos = New Collection
        _toneladas = New Collection
        mGestorViaje = New GestorViaje
    End Sub
    Sub New(ByVal fecha As Date)
        Me.Fecha = fecha
        _tren = New Tren
        _Productos = New Collection
        _toneladas = New Collection
        mGestorViaje = New GestorViaje
    End Sub
    Sub New(ByVal fecha As Date, ByVal matri As String, ByVal id As Integer, ByVal ton As Integer)
        _tren = New Tren
        _Productos = New Collection
        _toneladas = New Collection
        mGestorViaje = New GestorViaje
        mGestorViaje = New GestorViaje
        Me.Fecha = fecha
        Me.Tren.Matricula = matri
        Me.Productos.Add(New Producto(id))
        Me.Toneladas.Add(ton)
    End Sub
    Public Sub añadir()
        Me.Gestor.añadir(Me)
    End Sub
    Public Sub modificar()
        Me.Gestor.modificar(Me)
    End Sub
    Public Sub eliminar()
        Me.Gestor.eliminar(Me)
    End Sub
    Public Sub leerTodo()
        Me.Gestor.leerTodo()
    End Sub
    Public Sub leerFecha(ByVal fecha As Date)
        Me.Fecha = fecha
        Me.Gestor.leerFecha(Me)
    End Sub
    Public Sub leerFechaTren(ByVal fecha As Date, ByVal matr As String)
        Me.Tren.Matricula = matr
        Me.Fecha = fecha
        Me.Gestor.leerFechaTren(Me)
    End Sub
    Public Sub numViajes(ByVal fi As Date, ByVal ff As Date, ByRef num As Integer)
        Me.Gestor.numViajes(fi, ff, num, Me)
    End Sub
    Public Sub trenesFecha(ByVal fi As Date, ByVal ff As Date, ByRef tipo As TipoTren)
        Me.Gestor.TrenesEnFechasOrden(fi, ff, tipo)
    End Sub
    Public Sub productosFecha(ByVal fi As Date, ByVal ff As Date, ByRef prod As Producto)
        Me.Gestor.productosEnFechas(fi, ff, prod)
    End Sub
End Class
