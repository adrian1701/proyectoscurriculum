﻿Imports System.Data.OleDb
Imports System.Collections
Public Class GestorProducto
    Private listaProductos As Collection
    Private Ag As AgenteBD = AgenteBD.getAgente
    Sub New()
        listaProductos = New Collection
    End Sub
    Property Lista As Collection
        Get
            Return listaProductos
        End Get
        Set(value As Collection)
            listaProductos = value
        End Set
    End Property
    Public Sub leerTodo()
        Dim prod As Producto
        Dim lector As OleDbDataReader = Ag.leer("SELECT * FROM Productos ORDER BY IdProducto;")
        While lector.Read()
            prod = New Producto(lector(0), lector(1))
            Dim lector2 As OleDbDataReader = Ag.leer("SELECT Fecha, EurosPorTonelada FROM Cotizaciones WHERE Producto=" & prod.Id & " ;")
            While lector2.Read()
                prod.Cotizaciones.Add(New Cotizacion(lector2(0), lector2(1)))
            End While
            listaProductos.Add(prod)
        End While
        lector.Close()
    End Sub

    Public Sub leerProducto(ByRef prod As Producto)
        Dim id As Integer
        id = prod.Id
        Ag = AgenteBD.getAgente
        Dim lector As OleDbDataReader = Ag.leer("SELECT * FROM Productos WHERE IdProducto=" & id & ";")
        While lector.Read()
            prod.Descripcion = (lector(1))
        End While
        lector = Ag.leer("SELECT Fecha, EurosPorTonelada FROM Cotizaciones WHERE Producto =" & prod.Id & " ;")
        While lector.Read()
            prod.Cotizaciones.Add(New Cotizacion(lector(0), lector(1)))
        End While
        lector.Close()

    End Sub

    Public Sub añadir(ByRef prod As Producto)
        Ag = AgenteBD.getAgente
        Ag.modificar("INSERT INTO Productos VALUES (" & prod.Id & ", '" & prod.Descripcion & "');")
    End Sub
    Public Sub modificar(ByRef prod As Producto)
        Ag.modificar("UPDATE Productos SET DescripProducto='" & prod.Descripcion & "'WHERE IdProducto=" & prod.Id & ";")
    End Sub
    Public Sub eliminar(ByRef prod As Producto)
        Ag.modificar("DELETE FROM Productos WHERE IdProducto=" & prod.Id & ";")
    End Sub

End Class
