﻿Imports System.Data.OleDb
Imports Viajes.Inicio
Public Class AgenteBD

    Private Shared CadConexion = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" & rutaBD
    Private Shared mConexion As OleDbConnection
    Private Shared Instancia As AgenteBD

    Private Sub New()
        AgenteBD.mConexion = New OleDbConnection(CadConexion)
        AgenteBD.mConexion.Open()
    End Sub
    Public Shared Function getAgente() As AgenteBD
        'Mira si existe el agente y si no lo crea
        If Instancia Is Nothing Then
            Instancia = New AgenteBD
        End If
        Return Instancia
    End Function
    Public Function leer(ByVal sql As String) As OleDbDataReader
        Dim com As New OleDbCommand(sql, mConexion)
        Return com.ExecuteReader()
    End Function

    Public Function modificar(ByVal sql As String) As Integer
        Dim com As New OleDbCommand(sql, mConexion)
        Return com.ExecuteNonQuery

    End Function
End Class
