package practicaSistemas;

public class LugarGeografico {

	protected String hoja;
	protected int x;
	protected int y;
	public LugarGeografico(String hoja, int x, int y) {
		super();
		this.hoja = hoja;
		this.x = x;
		this.y = y;
	}
	public String getHoja() {
		return hoja;
	}
	public void setHoja(String hoja) {
		this.hoja = hoja;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	
}
