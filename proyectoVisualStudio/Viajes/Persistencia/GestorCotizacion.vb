﻿Imports System.Data.OleDb
Imports System.Collections
Public Class GestorCotizacion
    Private listaCotizaciones As Collection
    Private Ag As AgenteBD = AgenteBD.getAgente
    Public Sub New()
        listaCotizaciones = New Collection
    End Sub
    Property Lista As Collection
        Get
            Return listaCotizaciones
        End Get
        Set(value As Collection)

        End Set
    End Property
    Public Sub leerTodos()
        Ag = AgenteBD.getAgente()
        Dim cot As Cotizacion
        Dim lector As OleDbDataReader = Ag.leer("SELECT * FROM Cotizaciones ORDER BY Fecha;")
        While lector.Read()
            cot = New Cotizacion(lector(1), lector(2))
            cot.Producto.Id = lector(0)
            cot.Producto.leerProducto(cot.Producto.Id)
            listaCotizaciones.Add(cot)
        End While
        lector.Close()
    End Sub

    Public Sub leerCotizacion(ByRef cot As Cotizacion)
        Ag = AgenteBD.getAgente
        Dim lector As OleDbDataReader = Ag.leer("SELECT * FROM Cotizaciones WHERE Producto=" & cot.Producto.Id & ";")
        While lector.Read()
            cot.Fecha = lector(1)
            cot.Euros = lector(2)
            cot.Gestor.Lista.Add(cot)
        End While
        lector.Close()

    End Sub
    Public Sub leerCotizacionFecha(ByRef cot As Cotizacion)
        Ag = AgenteBD.getAgente
        Dim lector As OleDbDataReader = Ag.leer("SELECT EurosPorTonelada FROM Cotizaciones WHERE Producto=" & cot.Producto.Id & " AND Fecha=#" & cot.Fecha & "# ;")
        While lector.Read()
            cot.Euros = lector(0)
        End While
    End Sub
    Public Sub añadir(ByRef cot As Cotizacion) 'Hay que solucionar lo de los numeros decimales
        Ag.modificar("INSERT INTO Cotizaciones VALUES (" & cot.Producto.Id & ", #" & cot.Fecha & "#, " & cot.Euros & ");")
    End Sub
    Public Sub eliminar(ByRef cot As Cotizacion)
        Ag.modificar("DELETE FROM Cotizaciones WHERE Producto=" & cot.Producto.Id & "AND Fecha= #" & cot.Fecha & "#;")
    End Sub
    Public Sub modificar(ByRef cot As Cotizacion)
        Ag.modificar("UPDATE Cotizaciones SET Euros='" & cot.Euros & "'WHERE Producto=" & cot.Producto.Id & "AND Fecha= #" & cot.Fecha & "#;")
    End Sub

End Class

