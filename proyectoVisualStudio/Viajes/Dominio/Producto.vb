﻿Public Class Producto
    Private idProducto As Integer
    Private descripProducto As String
    Private _cotizaciones As Collection
    Private mGestorPro As GestorProducto
    Sub New()
        mGestorPro = New GestorProducto
        _cotizaciones = New Collection
    End Sub
    Sub New(ByVal id As Integer)
        Me.idProducto = id
        mGestorPro = New GestorProducto
        _cotizaciones = New Collection
    End Sub
    Sub New(ByVal id As Integer, ByVal descrip As String, ByRef cotis As Collection)
        Me.idProducto = id
        Me.Descripcion = descrip
        Me.cotizaciones = cotis
        mGestorPro = New GestorProducto
    End Sub
    Sub New(ByVal id As Integer, ByVal descrip As String)
        Me.Id = id
        Me.descripProducto = descrip
        mGestorPro = New GestorProducto
        _cotizaciones = New Collection
    End Sub
    Property Id As Integer
        Get
            Return idProducto
        End Get
        Set(ByVal value As Integer)
            idProducto = value
        End Set
    End Property
    Property Descripcion As String
        Get
            Return descripProducto
        End Get
        Set(ByVal value As String)
            descripProducto = value
        End Set
    End Property
    Property Cotizaciones As Collection
        Get
            Return _cotizaciones
        End Get
        Set(value As Collection)
            _cotizaciones = value
        End Set
    End Property
    Property Gestor As GestorProducto
        Get
            Return mGestorPro
        End Get
        Set(value As GestorProducto)

        End Set
    End Property
    Public Sub leerProducto(ByVal id As Integer)
        Me.Id = id
        Me.Gestor.leerProducto(Me)
    End Sub
    Public Sub leerTodo()
        Me.Gestor.leerTodo()
    End Sub
    Public Sub añadir()
        Me.Gestor.añadir(Me)
    End Sub
    Public Sub modificar()
        Me.Gestor.modificar(Me)
    End Sub
    Public Sub eliminar()
        Me.Gestor.eliminar(Me)
    End Sub
End Class