package practicaSistemas;

public class Estado {

	protected LugarGeografico lugar;
	protected float altura;
	protected String accion;
	public Estado(LugarGeografico lugar, String accion) {
		super();
		this.lugar = lugar;
		this.accion = accion;
		this.altura=Util.altura(lugar);
	}
	public LugarGeografico getLugar() {
		return lugar;
	}
	public void setLugar(LugarGeografico lugar) {
		this.lugar = lugar;
	}
	public float getAltura() {
		return altura;
	}
	public void setAltura(float altura) {
		this.altura = altura;
	}
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	public String toString() {
		return "Estado [lugar=" + lugar + ", altura=" + altura + ", accion="
				+ accion + "]";
	}
	
	
}
