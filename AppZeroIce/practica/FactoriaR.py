#!/usr/bin/python -u
# -*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('-I. --all RobotFactory.ice')
import drobots
import Services
import random


class RobotD(drobots.RobotController):
	
	def __init__(self,robot,proxyContainerRC,current = None):
		self.robot = robot
		self.proxyContainerRC = proxyContainerRC
		
		self.dirMovimiento = 0
		self.velocidad = self.robot.speed()

		
		self.anguloEscaneo = 0
		# la variable siguienteAccion  se usa para representar los diferentes estados

		self.siguienteAccion = "mover"

		# se la variable contadorDisparos para que no se quede
		# disparando a robots destruidos

		self.contadorDisparos = 0
		





	def turn(self,current=None):
		
		
		
		x = self.robot.location().x
		y = self.robot.location().y
		
		
		print("siguiente defender")
		print("Accion: "+self.siguienteAccion)
		print("x= " + str(x) +"," + "y= "+ str(y))
		
		
	
		if(self.siguienteAccion == "mover"):
			
			if ( x >=10 and x <= 500 and y >= 10 and y <=500 ):
				self.dirMovimiento = 90
				
				# si se acercan al cambio de direccion se reduce la velocidad
				# esto se hace en todos los movimientos de ambos robots.
				if ( y >=490 and y <=499):
					self.velocidad = 49
				else:
					self.velocidad = 100

			if( x >=10 and x <= 500 and y >= 500 and y <=990):	
				self.dirMovimiento = 0

				if ( x >=490 and x <=499):
					self.velocidad = 49
				else:
					self.velocidad = 100
				

			if( x >=500 and x <=990 and y >=500 and y <=990):
				self.dirMovimiento = 270
				
				if ( y <= 510 and y >=501):
					self.velocidad = 49
				else:
					self.velocidad = 100
				

			if ( x >= 500 and x <=990 and y >=10 and y <=500):
				self.dirMovimiento = 180

				if ( x <= 510 and x >= 501):
					self.velocidad = 49
				else:
					self.velocidad = 100


		



			if ( x < 10 and y > 10):
				self.dirMovimiento=0
				self.velocidad = 100

			if ( x > 10 and y < 10):
				self.dirMovimiento=90
				self.velocidad = 100

			if ( x < 10 and y < 10):
				self.dirMovimiento=45
				self.velocidad = 100

			if ( x >990 and y < 990):
				self.dirMovimiento = 180
				self.velocidad = 100
			
			if ( x < 990 and y > 990):
				self.dirMovimiento = 270
				self.velocidad = 100

			if ( x > 990 and y > 990):
				self.dirMovimiento = 225
				self.velocidad = 100
			

		

			self.robot.drive(self.dirMovimiento,self.velocidad)

			

		


		
		if (self.siguienteAccion == "moviendote" ):
 
			print("Buscando robots ...")
			
			# se escanea en diferentes angulos en busca de robots

			for i in range(9):
				
				
				robotEncontrado =self.robot.scan(self.anguloEscaneo, 20)
				if(robotEncontrado >= 1):
					# si se encuentra, en el siguiente turno se ocupara de disparar
					self.siguienteAccion = "prepararAtaque"
					break
				else:
					if(self.anguloEscaneo >= 360):
						self.anguloEscaneo = 22.5
					else:
						self.anguloEscaneo = self.anguloEscaneo + 45

			




		
		if ( self.siguienteAccion == "atacar"):
			
			self.robot.drive(0,0)
			
		
			# se busca en el contenedor de robotsCOntroller los robots
			# del tipo atacker para comunicarles que deben atacar
			
			print(" robot encontrado, apuntando..")
			com = current.adapter.getCommunicator()
			
       			proxysi = com.stringToProxy(self.proxyContainerRC)
        		container = Services.ContainerPrx.checkedCast(proxysi)

			RobotAttKeys = iter(container.list().keys())
			RobotAttProxies = iter(container.list().values())

			while True:
   				try:
					key = RobotAttKeys.next()
					proxie = RobotAttProxies.next()

    				except StopIteration:
        				break

				# la key es de tipo string por lo que es un vector de chars
				# el caracter 5 de la key  corresponde con la letra que identifica el tipo 
				# de robot, A para attacker y D para defender

				if key[5] == 'A':
					
					#se comunica a los atacantes que disparen

					ataca = drobots.RobotControllerPrx.checkedCast(proxie)
					ataca.dispararDonde(self.anguloEscaneo)
				





			robotEncontrado = self.robot.scan(self.anguloEscaneo,20)
			if ( robotEncontrado >= 1):
				self.contadorDisparos = self.contadorDisparos +1
				
				
				# 35 turnos atacando teniendo en cuenta el minimo dano,
				# es suficiente para destruir al robot tambien hay que tener en cuenta
				# que el robot se va acercando
				
				if (self.contadorDisparos > 35):
					
					robotEncontrado = 0
					self.anguloEscaneo = random.randrange(359)

			
		       




			if(robotEncontrado == 0):
				# si no encuentra robots o son basura, vuelve a moverse
				self.siguienteAccion = "remover"
				self.contadorDisparos = 0
				
				# se vuelven a buscar los atacantes en el contenedorRC
				# pero esta vez para ordenarles que paren de atacar

				container = Services.ContainerPrx.checkedCast(proxysi)
				RobotAttKeys = iter(container.list().keys())
				RobotAttProxies = iter(container.list().values())
				

				while True:
   					try:
						key = RobotAttKeys.next()
						proxie = RobotAttProxies.next()

    					except StopIteration:
        					break
					
					if key[5] == 'A':
					
						ataca = drobots.RobotControllerPrx.checkedCast(proxie)
						ataca.paraDisparar()

		




		# dependiendo del estado actual, el siguiente estado será consecuencia del mismo
		if(self.siguienteAccion == "mover"):
			self.siguienteAccion = "moviendote"
		if(self.siguienteAccion == "prepararAtaque"):
			self.siguienteAccion = "atacar"
		if(self.siguienteAccion == "remover"):
			self.siguienteAccion = "mover"






		vida = 100 - self.robot.damage()
		energia = self.robot.energy()
		print("energia " +str(energia) + " vida "+str(vida))
		print("")
		print("")
		print("")
						
				
				
				

	def robotDestroyed(self,current=None):
		print(" Robot defender  destruido")
		print("")







class RobotA(drobots.RobotController):
	

	def __init__(self,robot):
		self.robot = robot
		self.moverse = 1
		self.velocidad = self.robot.speed()
		self.dirMovimiento = 0
		self.disparar = 0
		self.angleDisp = 0
		self.distanciaDisparo = 81
		self.accionSiguiente = "mover"
		# la variable alternar se usa para acercarse al enemigo
		# en caso de que este demasiado lejos para acertarle

		self.alternar = 0
		


	
	def turn(self,current=None):

		
		
		

		print("siguiente attacker ")
		print("Acccion: "+self.accionSiguiente)

		
		

		if(self.moverse == 1 ):
			
			x = self.robot.location().x
			y = self.robot.location().y
			print("x= " + str(x) +"," + "y= "+ str(y))
			self.accionSiguiente = "mover"


			if ( x >=10 and x <= 500 and y >= 10 and y <=500 ):
				self.dirMovimiento = 45
					

				if ( y >=490 and y <=499 or x >= 490 and y <=499):
					self.velocidad = 49
				else:
					self.velocidad = 100

			if( x >=10 and x <= 500 and y >= 500 and y <=990):	
				self.dirMovimiento = 315


				if ( x >=490 and x <=499 or y >= 501 and y <=510 ):
					self.velocidad = 49
				else:
					self.velocidad = 100
				

			if( x >=500 and x <=990 and y >=500 and y <=990):
				self.dirMovimiento = 225


				if (x >=501 and x <=510 or y >=501 and y <=510 ):
					self.velocidad = 49
				else:
					self.velocidad = 100
				

			if ( x >= 500 and x <=990 and y >=10 and y <=500):
				self.dirMovimiento = 135
				self.velocidad = 100
				
				if ( x >= 501 and x <= 510 or y >=490 and y <=499):
					self.velocidad = 49
				else:
					self.velocidad = 100


		



			if ( x < 10 and y > 10):
				self.dirMovimiento=0
				self.velocidad = 100

			if ( x > 10 and y < 10):
				self.dirMovimiento=90
				self.velocidad = 100

			if ( x < 10 and y < 10):
				self.dirMovimiento=45
				self.velocidad = 100

			if ( x >990 and y < 990):
				self.dirMovimiento = 180
				self.velocidad = 100
			
			if ( x < 990 and y > 990):
				self.dirMovimiento = 270
				self.velocidad = 100

			if ( x > 990 and y > 990):
				self.dirMovimiento = 225
				self.velocidad = 100

			

		

			self.robot.drive(self.dirMovimiento,self.velocidad)

			energia = self.robot.energy()
			vida = 100 - self.robot.damage()
			print("energia " +str(energia)  + ", vida= "+str(vida))
			
		
		# va cambiando la distancia de disparo en cada turno

		if(self.disparar == 1 ):
			self.accionSiguiente = "atacar"
			
			if( self.alternar < 3):
				self.robot.cannon(self.angleDisp,self.distanciaDisparo)
				self.distanciaDisparo = self.distanciaDisparo + 100

				self.robot.cannon(self.angleDisp,self.distanciaDisparo)
				self.distanciaDisparo = self.distanciaDisparo + 100

				if ( self.distanciaDisparo > 700):
					self.distanciaDisparo = 81

			if(self.alternar >= 3):
				x = self.robot.location().x
				y = self.robot.location().y
				

				if ( x < 81 and y > 81):
					self.dirMovimiento=0
					self.velocidad = 100
					self.robot.drive(self.dirMovimiento,self.velocidad)

				if ( x > 81 and y < 81):
					self.dirMovimiento=90
					self.velocidad = 100
					self.robot.drive(self.dirMovimiento,self.velocidad)

				if ( x < 81 and y < 81):
					self.dirMovimiento=45
					self.velocidad = 100
					self.robot.drive(self.dirMovimiento,self.velocidad)

				if ( x >921 and y < 921):
					self.dirMovimiento = 180
					self.velocidad = 100
					self.robot.drive(self.dirMovimiento,self.velocidad)
			
				if ( x < 921 and y > 921):
					self.dirMovimiento = 270
					self.velocidad = 100
					self.robot.drive(self.dirMovimiento,self.velocidad)

				if ( x > 921 and y > 921):
					self.dirMovimiento = 225
					self.velocidad = 100
					self.robot.drive(self.dirMovimiento,self.velocidad)

				else:
					if(self.robot.energy() > 63):
						self.robot.drive(self.angleDisp,100)

				energia = self.robot.energy()
				vida = 100 - self.robot.damage()
				print("energia " +str(energia)  + ", vida= "+str(vida))
				self.alternar = 0

			self.alternar = self.alternar + 1



		
		print("")
		print("")
		print("")



		
	# este metodo recibe el angulo en el que debe disparar
	def dispararDonde(self,angle,current=None):
		self.disparar = 1
		self.moverse = 0
		self.robot.drive(0,0)
		
		self.angleDisp = angle


		
		


	#este metodo ordena al robot a parar de disparar y volver a moverse

	def paraDisparar(self,current=None):
		self.disparar = 0
		self.moverse = 1
		self.alternar = 0
        


	def robotDestroyed(self,current=None):
		print(" Robot attacker  destruido")
		print("")







class RobotFactoryI(drobots.RobotFactory):

	def __init__(self,containerProxies):
		# se usan atac y defen para permitir diferenciar el  nombre 
		# de  las keys del contenedor de robotControllers 
		self.atac = 0
		self.defen = 0
		self.containerProxies = containerProxies


		
		

	def make(self,name,current=None):
		
		com = current.adapter.getCommunicator()
				
		# se busca en el contenedor principal el contenedor de robots controllers

       		proxyContainerProxies = com.stringToProxy(self.containerProxies)
        	containerProxies = Services.ContainerPrx.checkedCast(proxyContainerProxies)

		keys = iter(containerProxies.list().keys())
		valores = iter(containerProxies.list().values())
			
		while True:
   			try:
				key = keys.next()
				proxies = valores.next()

    			except StopIteration:
        			break
				
			if key == "proxyContainerRC":
				
				containerRC = Services.ContainerPrx.checkedCast(proxies)
				proxyContainerRC = com.proxyToString(proxies)
					

		
		#se crean sirvientes a traves del proxy que se pasa en el contructor

		if name.ice_isA("::drobots::Attacker"):
			servant = RobotA(name)
			self.atac = self.atac +1
			nombre = "RobotA" + str(self.atac)
			
			
		# se le pasa el proxy del contenedor de Robotcontroller a los robots defenders para
		# poder comunicarse con los robots atacker

		if name.ice_isA("::drobots::Defender"):
			servant = RobotD(name,proxyContainerRC)
			self.defen = self.defen +1
			nombre = "RobotD" + str(self.defen)
		
		
		
		
		# se almacenan los proxys de los robots controllers en el 
		#contenedor de robotsController asociandole a cada uno un
		#nombre unico

		proxyRC = current.adapter.addWithUUID(servant)
		containerRC.link(nombre,proxyRC)
		
		return drobots.RobotControllerPrx.checkedCast(proxyRC)






class Server(Ice.Application):
    def run(self, argv):

	
        broker = self.communicator()
	
	# cogemos el proxy del contenedor principal para pasarselo como argumento al sirviente de 
	# la factoria de robots

	ContainerProxies = argv[1]
        servant = RobotFactoryI(ContainerProxies)

	# se crea  el proxy de la factoria

        adapter = broker.createObjectAdapterWithEndpoints("RobotFactoryAdapter","default")
        proxyF = adapter.addWithUUID(servant)
	adapter.activate()
                           
	
        sys.stdout.flush()
	
	# se crea un objeto contenedor a traves del proxy del contenedor
	# principal que se pasa como argumento y se almacena el proxy de la factoria
	
	proxyContainerProxies = broker.stringToProxy(argv[1])
        container = Services.ContainerPrx.checkedCast(proxyContainerProxies)
	nombre = "proxieFactoria"
	container.link(nombre,proxyF)

	
	
	

       
        self.shutdownOnInterrupt()
        broker.waitForShutdown()


        return 0


server = Server()
sys.exit(server.main(sys.argv))




