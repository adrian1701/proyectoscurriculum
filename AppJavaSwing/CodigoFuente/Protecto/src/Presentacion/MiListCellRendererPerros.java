package Presentacion;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;


	class MiListCellRendererPerros extends DefaultListCellRenderer {
		protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
		ImageIcon iconoLabrador;
		ImageIcon iconoGalgo;
		ImageIcon iconoBorder;
		ImageIcon iconoMestizo;
		ImageIcon iconoPastor;
		public MiListCellRendererPerros () {
		iconoLabrador = new ImageIcon(MiListCellRendererPerros.class.getResource("labrador.jpg"));
		iconoMestizo = new ImageIcon(MiListCellRendererPerros.class.getResource("mestizo.jpg"));
		iconoGalgo= new ImageIcon(MiListCellRendererPerros.class.getResource("galgo.jpg"));
		iconoBorder = new ImageIcon(MiListCellRendererPerros.class.getResource("borderCollie.jpg"));
		iconoPastor = new ImageIcon(MiListCellRendererPerros.class.getResource("pastorAleman.jpg"));
		}
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasFocus)
		{
		JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, hasFocus);
		if (list.getModel().getElementAt(index) == "Perry") {
		renderer.setIcon(iconoLabrador);
		} else
		if (list.getModel().getElementAt(index) == "Guagua"){
		renderer.setIcon(iconoMestizo);
		}else
		if (list.getModel().getElementAt(index) == "Juan") {
			renderer.setIcon(iconoGalgo);
		} else
		if (list.getModel().getElementAt(index) == "Perro") {
			renderer.setIcon(iconoBorder);
		} else
			if (list.getModel().getElementAt(index) == "Perro2") {
			renderer.setIcon(iconoPastor);
				} 
	
		return renderer;
		}
		}