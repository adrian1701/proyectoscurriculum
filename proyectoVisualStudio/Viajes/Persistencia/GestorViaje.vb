﻿Imports System.Data.OleDb
Imports System.Collections
Public Class GestorViaje
    Private listaViaje As Collection
    Private Ag As AgenteBD = AgenteBD.getAgente
    Property Lista As Collection
        Get
            Return listaViaje
        End Get
        Set(value As Collection)

        End Set
    End Property
    Sub New()
        listaViaje = New Collection
    End Sub

    Public Sub añadir(ByRef viaje As Viaje)
        Ag.modificar("INSERT INTO viajes VALUES (#" & viaje.Fecha & "#, '" & viaje.Tren.Matricula & "', " & viaje.Productos(1).Id & ", " & viaje.Toneladas(1) & ");")
    End Sub
    Public Sub modificar(ByRef viaje As Viaje)
        Ag.modificar("UPDATE viajes SET ToneladasTransportadas=" & viaje.Toneladas(1) & " WHERE  FechaViaje =#" & viaje.Fecha & "# AND Tren='" & viaje.Tren.Matricula & "' AND Producto=" & viaje.Productos(1).id & ";")
    End Sub
    Public Sub eliminar(ByRef viaje As Viaje)
        Ag.modificar("DELETE FROM viajes WHERE FechaViaje=#" & viaje.Fecha & "# AND Tren='" & viaje.Tren.Matricula & "' AND Producto=" & viaje.Productos(1).id & " ;")
    End Sub
    Public Sub leerTodo()
        Ag = AgenteBD.getAgente()
        Dim via As Viaje
        Dim lector As OleDbDataReader = Ag.leer("SELECT FechaViaje, Tren FROM viajes ORDER BY FechaViaje ;")
        While lector.Read()
            Dim fecha As Date = lector(0)
            via = New Viaje(fecha)
            via.Tren.Matricula = lector(1)
            via.Tren.leerMatricula(via.Tren.Matricula)

            Dim lector2 As OleDbDataReader = Ag.leer("SELECT Producto, ToneladasTransportadas FROM viajes WHERE FechaViaje =#" & lector(0) & "# AND Tren ='" & lector(1) & "'  ;")
            While lector2.Read()
                Dim prod = New Producto(lector2(0))
                prod.leerProducto(prod.Id)
                via.Productos.Add(prod)
                Dim ton As Integer
                ton = lector2(1)
                via.Toneladas.Add(ton)
            End While

            listaViaje.Add(via)
        End While
        lector.Close()

    End Sub
    Public Sub leerFecha(ByRef viajeAux As Viaje)
        Ag = AgenteBD.getAgente()
        Dim listaProductos As Collection = New Collection
        Dim listaToneladas As Collection = New Collection
        Dim lector As OleDbDataReader = Ag.leer("SELECT tren, producto, toneladastransportadas FROM viajes WHERE FechaViaje= #" & viajeAux.Fecha & "# ;")
        While lector.Read()
            viajeAux.Tren.Matricula = lector(0)
            Dim prod = New Producto(lector(1))
            listaProductos.Add(prod)
            Dim ton As Integer = lector(2)
            listaToneladas.Add(ton)
        End While
        viajeAux.Productos = listaProductos
        viajeAux.Toneladas = listaToneladas
        lector.Close()
    End Sub
    Public Sub leerFechaTren(ByRef viajeAux As Viaje)
        Ag = AgenteBD.getAgente()
        Dim listaProductos As Collection = New Collection
        Dim listaToneladas As Collection = New Collection
        Dim lector As OleDbDataReader = Ag.leer("SELECT tren, producto, toneladastransportadas FROM viajes WHERE FechaViaje= #" & viajeAux.Fecha & "# AND Tren ='" & viajeAux.Tren.Matricula & "' ;")
        While lector.Read()
            viajeAux.Tren.Matricula = lector(0)
            Dim prod = New Producto(lector(1))
            listaProductos.Add(prod)
            Dim ton As Integer = lector(2)
            listaToneladas.Add(ton)
        End While
        viajeAux.Productos = listaProductos
        viajeAux.Toneladas = listaToneladas
        lector.Close()
    End Sub
    Sub numViajes(ByVal fi As Date, ByVal ff As Date, ByRef num As Integer, ByRef viajeAux As Viaje)
        Ag = AgenteBD.getAgente()
        Dim lector As OleDbDataReader = Ag.leer("SELECT COUNT(*) FROM Viajes WHERE Tren = '" & viajeAux.Tren.Matricula & "' AND FechaViaje BETWEEN #" & fi & "# AND #" & ff & "#;")

        While lector.Read()
            MessageBox.Show(lector(0))
            num = lector(0)
        End While
        lector = Ag.leer("SELECT Producto FROM Viajes WHERE Tren = '" & viajeAux.Tren.Matricula & "' AND FechaViaje BETWEEN #" & fi & "# AND #" & ff & "#;")

        While lector.Read()
            Dim prodAux As Producto = New Producto(lector(0))
            viajeAux.Productos.Add(prodAux)
        End While
    End Sub
    Sub TrenesEnFechasOrden(ByVal fi As Date, ByVal ff As Date, ByRef tipo As TipoTren) 'ByVal tipo As TipoTren)
        Ag = AgenteBD.getAgente()
        Dim SQL As String = "SELECT tren.tipotren, COUNT(*) " +
                            "FROM Viajes v, Trenes tren " +
                            "WHERE tren.matricula = v.tren AND v.FechaViaje BETWEEN #" & fi & "# AND #" & ff & "#" +
                            "GROUP BY tren.tipotren ORDER BY COUNT(*) DESC;"
        Dim lector As OleDbDataReader = Ag.leer(SQL)
        While lector.Read()
            tipo.Gestor.Lista.Add(New TipoTren(lector(0)))
        End While
    End Sub
    Sub productosEnFechas(ByVal fi As Date, ByVal ff As Date, ByRef prod As Producto)
        Ag = AgenteBD.getAgente()
        'Dim SQL As String = "SELECT producto, COUNT(*) AS RANKING FROM Viajes WHERE FechaViaje BETWEEN #" & fi & "# AND #" & ff & "# GROUP BY producto ORDER BY RANKING DESC;"
        Dim lector As OleDbDataReader = Ag.leer("SELECT Producto, COUNT(*)  FROM Viajes WHERE FechaViaje BETWEEN #" & fi & "# AND #" & ff & "# GROUP BY Producto ORDER BY COUNT(*) DESC;")
        While lector.Read()
            prod.Gestor.Lista.Add(New Producto(lector(0)))
        End While
    End Sub
    Sub viajeMasRentable(ByVal v As Viaje)
        Ag = AgenteBD.getAgente()
        Dim SQL As String = "SELECT v.FechaViaje, v.Tren, v.Producto, v.ToneladasTransportadas " +
                            "FROM Viajes v, Productos p, Cotizaciones c " +
                            "WHERE v.producto = p.idproducto AND p.idproducto = c.producto AND  v.toneladastransportadas*c.eurosportonelada >= ALL( " +
                                "SELECT v.toneladastransportadas*c.eurosportonelada " +
                                "FROM Viajes v, Productos p, Cotizaciones c " +
                                "WHERE v.producto = p.idproducto AND p.idproducto = c.producto " +
                            ");"
        Dim lector As OleDbDataReader = Ag.leer(SQL)
        MessageBox.Show(lector(0) & " " & lector(1) & " " & lector(2) & " " & lector(3))
    End Sub
End Class
