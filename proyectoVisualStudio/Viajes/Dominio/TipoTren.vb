﻿Public Class TipoTren
    Private idTipoTren As Integer
    Private descripTren As String
    Private mGestorTipo As GestorTipoTren
    Property TipoTren As Integer
        Get
            Return idTipoTren
        End Get
        Set(ByVal value As Integer)
            idTipoTren = value
        End Set
    End Property
    Property Descripcion As String
        Get
            Return descripTren
        End Get
        Set(ByVal value As String)
            descripTren = value
        End Set
    End Property
    Property Gestor As GestorTipoTren
        Get
            Return mGestorTipo
        End Get
        Set(value As GestorTipoTren)

        End Set
    End Property
    Sub New()
        mGestorTipo = New GestorTipoTren
    End Sub
    Sub New(ByVal id As Integer, ByVal descrip As String)
        Me.TipoTren = id
        Me.Descripcion = descrip
        mGestorTipo = New GestorTipoTren
    End Sub
    Sub New(ByVal id As Integer)
        Me.TipoTren = id
        mGestorTipo = New GestorTipoTren
    End Sub
    Public Sub leerTipo(ByVal id As Integer)
        Me.TipoTren = id
        Me.Gestor.leerTipo(Me)
    End Sub
    Public Sub leerTodos()
        Me.Gestor.leerTodos()
    End Sub
    Public Sub añadir()
        Me.Gestor.añadir(Me)
    End Sub
    Public Sub modificar()
        Me.Gestor.modificar(Me)
    End Sub
    Public Sub eliminar()
        Me.Gestor.eliminar(Me)
    End Sub
End Class
