﻿Imports System.Data.OleDb
Imports System.Collections
Public Class GestorTipoTren
    Private listaTipoTren As Collection
    Private Ag As AgenteBD = AgenteBD.getAgente
    Property Lista As Collection
        Get
            Return listaTipoTren
        End Get
        Set(value As Collection)

        End Set
    End Property
    Sub New()
        listaTipoTren = New Collection
    End Sub
    Public Sub leerTodos()
        Ag = AgenteBD.getAgente()
        Dim tipo As TipoTren
        Dim lector As OleDbDataReader = Ag.leer("SELECT * FROM tipos_tren ORDER BY IdTipoTren;")
        While lector.Read()
            tipo = New TipoTren(lector(0), lector(1))
            listaTipoTren.Add(tipo)
        End While
        lector.Close()
    End Sub
    Public Sub leerTipo(ByRef tipo As TipoTren)
        Dim id As Integer
        id = tipo.TipoTren
        Ag = AgenteBD.getAgente
        Dim lector As OleDbDataReader = Ag.leer("SELECT * FROM tipos_tren WHERE IdTipoTren=" & id & ";")
        While lector.Read()
            tipo.Descripcion = (lector(1))
        End While
        lector.Close()

    End Sub
    Public Sub añadir(ByRef tipo As TipoTren)
        Ag = AgenteBD.getAgente
        Ag.modificar("INSERT INTO tipos_tren VALUES (" & tipo.TipoTren & ", '" & tipo.Descripcion & "');")
    End Sub
    Public Sub modificar(ByRef tipo As TipoTren)
        Ag.modificar("UPDATE tipos_tren SET DescTipoTren='" & tipo.Descripcion & "'WHERE IdTipoTren=" & tipo.TipoTren & ";")
    End Sub
    Public Sub eliminar(ByRef tipo As TipoTren)
        Ag.modificar("DELETE FROM tipos_tren WHERE IdTipoTren=" & tipo.TipoTren & ";")
    End Sub
End Class
