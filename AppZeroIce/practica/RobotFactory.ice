#include <drobots.ice>

module drobots {
  interface RobotFactory {
   RobotController* make(Robot* name);
  };
};
