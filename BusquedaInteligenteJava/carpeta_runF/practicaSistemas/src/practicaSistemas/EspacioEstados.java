package practicaSistemas;

import java.util.ArrayList;


public class EspacioEstados {
	
	public Estado getEstado(LugarGeografico lugar){
		Estado estado;
		int[] utm = Util.utm(lugar);
		lugar=Util.lugarGeografico(utm[0], utm[1]);
		estado=new Estado(lugar, "Origen");
		
		return estado;
	}
	
	public static ArrayList<Sucesor> sucesores(Estado inicial){
		ArrayList<Sucesor>camino=new ArrayList<Sucesor>();
		
		
		int[] utm = Util.utm(inicial.getLugar());
		int pos =0;
		String[] cardinales = {"SO", "O", "NO", "S", "N", "SE", "E", "NE"};
		
		Sucesor suc=null;
		
		for(int i=-1; i<2;i++){
			for(int j=-1; j<2; j++){
				if(j==0 && i==0) continue;
				
				LugarGeografico lug = Util.lugarGeografico(utm[0]+(25*i), utm[1]+(25*j));
				
				if(lug != null){
					Estado estado = new Estado(lug, devolverAcceso(cardinales[pos]));
					//coge cada posici�n de cardinales y genera su opuesto mediante el metodo.
					suc=new Sucesor(estado,cardinales[pos],Math.abs(estado.getAltura()-inicial.getAltura()));
					camino.add(suc);
					++pos;
				}
				else{
					++pos;
				}
				
			}
		}		
		return camino;
		
	}
	
	public static String devolverAcceso(String anterior){
		
		String valor = null;			
		
		switch(anterior){
		case "SO":
			valor="NE";
			break;
			
		case "O":
			valor="E";
			break;
			
		case "NO":
			valor="SE";
			break;
		case "S":
			valor="N";
			break;
			
		case "N":
			valor="S";
			break;
			
		case "SE":
			valor="NO";
			break;
			
		case "E":
			valor="O";
			break;
		
		case "NE":
			valor="SO";
			break;
		
		}
	
		
		return valor;
	}
}
