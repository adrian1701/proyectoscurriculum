﻿Public Class Principal
    Private tipoTren As TipoTren
    Private tren As Tren
    Private cotizacion As Cotizacion
    Private producto As Producto
    Private viaje As Viaje

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Cargamos la lista de los tipos de trenes 
        Dim tipoAux As TipoTren = New TipoTren
        tipoTren = New TipoTren
        Try
            tipoTren.leerTodos()
            For Each tipoAux In tipoTren.Gestor.Lista
                ListBoxTipo.Items.Add(tipoAux.TipoTren)
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        'Cargamos la lista de los trenes y la lista de consultas
        tren = New Tren
        Dim trenAux As Tren = New Tren
        Try
            tren.leerTodo()
            For Each trenAux In tren.Gestor.Lista
                ListBoxTren.Items.Add(trenAux.Matricula)
                ListBoxTrenConsul.Items.Add(trenAux.Matricula)
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        'Cargamos la lista de las cotizaciones
        cotizacion = New Cotizacion
        Try
            cotizacion.leerTodo()

            For Each cotAux As Cotizacion In cotizacion.Gestor.Lista

                If ListBoxCoti.Items.Contains(cotAux.Producto.Descripcion) Then
                Else
                    ListBoxCoti.Items.Add(cotAux.Producto.Descripcion)
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        'Cargamos la lista de los productos
        producto = New Producto
        Dim prodAux As Producto = New Producto
        Try
            producto.leerTodo()
            For Each prodAux In producto.Gestor.Lista
                ListBoxProducto.Items.Add(prodAux.Descripcion)
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        'Cargamos la lista de los Viajes
        viaje = New Viaje
        Dim viajeAux As Viaje = New Viaje
        Try
            viaje.leerTodo()
            For Each viajeAux In viaje.Gestor.Lista
                If ListBoxViaje.Items.Contains(viajeAux.Fecha) Then

                Else
                    ListBoxViaje.Items.Add(viajeAux.Fecha)
                End If

            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    'Metodos de tren 
    Private Sub ListBoxTren_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxTren.SelectedIndexChanged 'Seleccionar un elemento de la lista de Trenes
        Dim matricula As String = ListBoxTren.SelectedItem
        Dim tren As Tren = New Tren
        Try
            tren.leerMatricula(matricula)
            TextBox1.Text = tren.Matricula
            TextBox2.Text = tren.Tipo.TipoTren
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click ' Añadir Tren
        Dim trenA As Tren = New Tren
        Try
            trenA.Matricula = TextBox1.Text
            trenA.Tipo.TipoTren = TextBox2.Text
            trenA.Tipo.leerTipo(trenA.Tipo.TipoTren)
            trenA.añadir()
            ListBoxTren.Items.Add(trenA.Matricula)
            tren.Gestor.Lista.add(trenA)
            ListBoxTren.ClearSelected()
            TextBox1.Text = ""
            TextBox2.Text = ""
        Catch ex As Exception
            ListBoxTren.ClearSelected()
            TextBox1.Text = ""
            TextBox2.Text = ""
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click ' Modificar Tren
        Dim tren As Tren = New Tren(TextBox1.Text)
        Try
            tren.Tipo.TipoTren = TextBox2.Text
            tren.modificar()
            ListBoxTren.ClearSelected()
            TextBox1.Text = ""
            TextBox2.Text = ""
        Catch ex As Exception
            ListBoxTren.ClearSelected()
            TextBox1.Text = ""
            TextBox2.Text = ""
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click 'Eliminar Tren
        If MessageBox.Show("¿Está seguro de querer eliminarlo?", "Eliminar", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.OK Then
            Try
                Dim tren As Tren = New Tren(TextBox1.Text)
                tren.eliminar()
                ListBoxTren.Items.Remove(ListBoxTren.SelectedItem)
                ListBoxTren.ClearSelected()
                TextBox1.Text = ""
                TextBox2.Text = ""
            Catch ex As Exception
                ListBoxTren.ClearSelected()
                TextBox1.Text = ""
                TextBox2.Text = ""
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click ' Limpiar campos Tren
        TextBox1.Text = ""
        TextBox2.Text = ""
    End Sub

    'Metodos de tipo tren
    Private Sub ListBoxTipo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxTipo.SelectedIndexChanged
        Dim idTipo As Integer = ListBoxTipo.SelectedItem
        Dim tipo As TipoTren = New TipoTren
        Try
            tipo.leerTipo(idTipo)
            TextBox3.Text = tipo.TipoTren
            TextBox4.Text = tipo.Descripcion
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click 'Añadir tipo tren
        Try
            Dim tipo As TipoTren = New TipoTren(TextBox3.Text, TextBox4.Text)
            tipo.añadir()
            ListBoxTipo.Items.Add(TextBox3.Text)
            tipoTren.Gestor.Lista.Add(tipo)
            ListBoxTipo.ClearSelected()
            TextBox3.Text = ""
            TextBox4.Text = ""
        Catch ex As Exception
            ListBoxTipo.ClearSelected()
            TextBox3.Text = ""
            TextBox4.Text = ""
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click 'Modificar tipo tren
        Try
            Dim tipo As TipoTren = New TipoTren(TextBox3.Text, TextBox4.Text)
            If TextBox4.Text.Length = 0 Then
                MessageBox.Show("Introduzca una descripción válida del Tipo del Tren")
                ListBoxProducto.ClearSelected()
                TextBox5.Text = ""
                TextBox6.Text = ""
            Else
                tipo.modificar()
                ListBoxTipo.ClearSelected()
                TextBox3.Text = ""
                TextBox4.Text = ""
            End If

        Catch ex As Exception
            ListBoxTipo.ClearSelected()
            TextBox3.Text = ""
            TextBox4.Text = ""
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click 'Eliminar tipo tren
        If MessageBox.Show("¿Está seguro de querer eliminarlo?", "Eliminar", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.OK Then
            Try
                Dim tipo As TipoTren = New TipoTren(TextBox3.Text, TextBox4.Text)
                tipo.eliminar()
                ListBoxTipo.Items.Remove(ListBoxTipo.SelectedItem)
                ListBoxTipo.ClearSelected()
                TextBox3.Text = ""
                TextBox4.Text = ""
            Catch ex As Exception
                ListBoxTipo.ClearSelected()
                TextBox3.Text = ""
                TextBox4.Text = ""
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        TextBox3.Text = ""
        TextBox4.Text = ""
    End Sub

    'Metodos de Producto
    Private Sub ListBoxProducto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxProducto.SelectedIndexChanged 'Lista Productos
        Dim nombre As String = ListBoxProducto.SelectedItem
        Dim id As Integer
        Dim prodAux As Producto = New Producto
        For Each prodAux In producto.Gestor.Lista
            If prodAux.Descripcion = nombre Then
                id = prodAux.Id
            End If
        Next
        Dim prod As Producto = New Producto
        Try
            prod.leerProducto(id)
            TextBox5.Text = prod.Id
            TextBox6.Text = prod.Descripcion
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click 'Añadir Producto
        Dim prod As Producto = New Producto(TextBox5.Text, TextBox6.Text)
        Try
            If TextBox6.Text.Length = 0 Or ListBoxProducto.Items.Contains(TextBox6.Text) Then
                MessageBox.Show("Introduzca una descripción válida del producto")
                ListBoxProducto.ClearSelected()
                TextBox5.Text = ""
                TextBox6.Text = ""
            Else
                prod.añadir()
                ListBoxProducto.Items.Add(prod.Descripcion)
                ListBoxCoti.Items.Add(prod.Descripcion)
                producto.Gestor.Lista.Add(prod)
                ListBoxProducto.ClearSelected()
                TextBox5.Text = ""
                TextBox6.Text = ""
            End If
        Catch ex As Exception
            ListBoxProducto.ClearSelected()
            TextBox5.Text = ""
            TextBox6.Text = ""
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Try
            Dim prod As Producto = New Producto(TextBox5.Text, TextBox6.Text)
            If TextBox6.Text.Length = 0 Then
                MessageBox.Show("Introduzca una descripción válida del producto")
                ListBoxProducto.ClearSelected()
                TextBox5.Text = ""
                TextBox6.Text = ""
            Else
                prod.modificar()
                TextBox5.Text = ""
                TextBox6.Text = ""
            End If

        Catch ex As Exception
            ListBoxProducto.ClearSelected()
            TextBox5.Text = ""
            TextBox6.Text = ""
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click 'Eliminar producto
        If MessageBox.Show("¿Está seguro de querer eliminarlo?", "Eliminar", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.OK Then
            Try
                Dim prod As Producto = New Producto(TextBox5.Text, TextBox6.Text)
                prod.eliminar()
                ListBoxProducto.Items.Remove(ListBoxProducto.SelectedItem)
                ListBoxProducto.ClearSelected()
                TextBox5.Text = ""
                TextBox6.Text = ""
            Catch ex As Exception
                ListBoxProducto.ClearSelected()
                TextBox5.Text = ""
                TextBox6.Text = ""
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click 'Limpiar campos producto
        TextBox5.Text = ""
        TextBox6.Text = ""
    End Sub

    'Metodos Cotizacion
    Private Sub ListBoxCoti_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxCoti.SelectedIndexChanged
        Dim prod As Producto = New Producto
        Dim descri As String
        ListBoxFechas.Items.Clear()
        descri = ListBoxCoti.SelectedItem
        prod.Descripcion = descri
        For Each prodAux As Producto In producto.Gestor.Lista
            If prodAux.Descripcion = descri Then
                prod.Id = prodAux.Id
            End If
        Next

        Dim cotAux As Cotizacion = New Cotizacion

        Try
            cotAux.leerCotizacion(prod)
            For Each cotAux2 As Cotizacion In cotAux.Gestor.Lista
                If Not ListBoxFechas.Items.Contains(cotAux.Fecha) Then
                    ListBoxFechas.Items.Add(cotAux.Fecha)
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ListBoxFechas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxFechas.SelectedIndexChanged
        Dim coti As Cotizacion = New Cotizacion
        Dim prod As Producto = New Producto
        Dim descri As String
        descri = ListBoxCoti.SelectedItem
        prod.Descripcion = descri
        Dim fecha As Date = ListBoxFechas.SelectedItem
        DateTimePicker1.Value = fecha
        For Each prodAux As Producto In producto.Gestor.Lista
            If prodAux.Descripcion = descri Then
                prod.Id = prodAux.Id
            End If
        Next

        Try
            coti.leerCotizacionFecha(prod, fecha)
            TextBox9.Text = coti.Euros
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub Button14_Click(sender As Object, e As EventArgs) Handles Button14.Click 'Añadir Cotizacion
        Dim prod As Producto = New Producto
        Dim descr As String = ListBoxCoti.SelectedItem
        Dim fecha As Date = DateTimePicker1.Value.ToShortDateString
        Dim eur As Double = TextBox9.Text
        For Each prodAux In producto.Gestor.Lista
            If prodAux.Descripcion = descr Then
                prod.Id = prodAux.id
                prod.Descripcion = descr
            End If
        Next
        Dim coti As Cotizacion = New Cotizacion(prod, fecha, eur)
        Try
            coti.añadir()
            If ListBoxCoti.Items.Contains(coti.Producto.Descripcion) Then
                cotizacion.Gestor.Lista.Add(coti)

            End If
            ListBoxCoti.Items.Add(coti.Producto.Descripcion)
            cotizacion.Gestor.Lista.Add(coti)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button15_Click(sender As Object, e As EventArgs) Handles Button15.Click 'Modificar cotización
        Dim prod As Producto = New Producto
        Dim descrip As String = ListBoxCoti.SelectedItem
        Dim fecha As Date = DateTimePicker1.Value.ToString
        Dim eur As Double = TextBox9.Text
        Dim coti As Cotizacion = New Cotizacion(prod, fecha, eur)

        Try
            coti.modificar()
            coti.Fecha = DateTimePicker1.Value
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button16_Click(sender As Object, e As EventArgs) Handles Button16.Click 'Eliminar Cotización
        Dim coti As Cotizacion = New Cotizacion(ListBoxFechas.SelectedItem, TextBox9.Text)
        coti.Producto.Id = ListBoxCoti.SelectedItem.Text
        coti.Fecha = DateTimePicker1.Text
        If MessageBox.Show("¿Está seguro de querer eliminarlo?", "Eliminar", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.OK Then
            Try
                coti.eliminar()
                ListBoxFechas.Items.Remove(ListBoxFechas.SelectedItem)
                TextBox9.Text = ""
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
    Private Sub Button17_Click(sender As Object, e As EventArgs) Handles Button17.Click 'Limpiar campos cotización
        TextBox9.Text = ""
        ListBoxFechas.Items.Clear()
    End Sub

    'Metodos Viaje
    Private Sub ListBoxViaje_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxViaje.SelectedIndexChanged
        ListBoxVProducto.Items.Clear()
        ListBoxVTrenes.Items.Clear()
        Dim fecha As Date = ListBoxViaje.SelectedItem
        Dim tren As Tren = New Tren
        Try
            DateTimePicker2.Value = fecha
            viaje.leerFecha(fecha)
            For Each viajeAux As Viaje In viaje.Gestor.Lista
                If viajeAux.Fecha = fecha Then
                    If ListBoxVTrenes.Items.Contains(viajeAux.Tren.Matricula) Then
                    Else
                        ListBoxVTrenes.Items.Add(viajeAux.Tren.Matricula)
                    End If
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ListBoxVTrenes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxVTrenes.SelectedIndexChanged
        ListBoxVProducto.Items.Clear()
        Dim matr As String = ListBoxVTrenes.SelectedItem
        Dim fecha As Date = ListBoxViaje.SelectedItem
        Dim viajeAux As Viaje = New Viaje
        Dim prodAux As Producto = New Producto
        Try
            viajeAux.leerFechaTren(fecha, matr)
            For Each prodAux In viajeAux.Productos
                prodAux.leerProducto(prodAux.Id)
                ListBoxVProducto.Items.Add(prodAux.Descripcion)
            Next
            TextBox8.Text = matr
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ListBoxVProducto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxVProducto.SelectedIndexChanged
        Dim descr As String = ListBoxVProducto.SelectedItem
        Dim matr As String = ListBoxVTrenes.SelectedItem
        Dim fecha As Date = ListBoxViaje.SelectedItem
        Dim viajeAux As Viaje = New Viaje
        Dim prodAux As Producto = New Producto
        Dim i As Integer = 0
        Dim pos, id As Integer
        TextBox10.Text = descr
        For Each prodAux In producto.Gestor.Lista
            If prodAux.Descripcion = descr Then
                id = prodAux.Id
            End If
        Next
        Try
            viajeAux.leerFechaTren(fecha, matr)
            For Each prodAux In viajeAux.Productos
                i = i + 1
                If prodAux.Id = id Then
                    pos = i
                    TextBox11.Text = viajeAux.Toneladas(pos)
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button18_Click(sender As Object, e As EventArgs) Handles Button18.Click 'Añadir viaje
        Dim fecha As Date = DateTimePicker2.Value
        Dim tren As String = TextBox8.Text
        Dim descr As String = TextBox10.Text
        Try
            Dim tonelada As Integer = TextBox11.Text
            Dim id As Integer
            For Each prodAux As Producto In producto.Gestor.Lista
                If prodAux.Descripcion = descr Then
                    id = prodAux.Id
                End If
            Next
            Dim viajeAux As Viaje = New Viaje(fecha, tren, id, tonelada)
            viajeAux.añadir()
            viaje.Gestor.Lista.Add(viajeAux)
            If Not ListBoxViaje.Items.Contains(viajeAux.Fecha) Then
                ListBoxViaje.Items.Add(viajeAux.Fecha)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click 'Modificar viaje
        Dim fecha As Date = DateTimePicker2.Value
        Dim tren As String = TextBox8.Text
        Dim descr As String = TextBox10.Text
        Try
            Dim tonelada As Integer = TextBox11.Text
            Dim id As Integer
            For Each prodAux As Producto In producto.Gestor.Lista
                If prodAux.Descripcion = descr Then
                    id = prodAux.Id
                End If
            Next
            Dim viajeAux As Viaje = New Viaje(fecha, tren, id, tonelada)

            viajeAux.modificar()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Button20_Click(sender As Object, e As EventArgs) Handles Button20.Click ' Eliminar Viaje
        If MessageBox.Show("¿Está seguro de querer eliminarlo?", "Eliminar", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.OK Then

            Dim fecha As Date = DateTimePicker2.Value
            Dim tren As String = TextBox8.Text
            Dim descr As String = TextBox10.Text
            Dim id As Integer
            For Each prodAux As Producto In producto.Gestor.Lista
                If prodAux.Descripcion = descr Then
                    id = prodAux.Id
                End If
            Next
            Dim viajeAux As Viaje = New Viaje()
            viajeAux.Fecha = fecha
            viajeAux.Tren.Matricula = tren
            viajeAux.Productos.Add(New Producto(id))
            Try
                viajeAux.eliminar()
                ListBoxVProducto.Items.Remove(descr)
                'Si se queda sin productos el tren, se elimina el tren de la lista
                If ListBoxVProducto.Items.Count = 0 Then
                    ListBoxVTrenes.Items.Remove(tren)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
    Private Sub Button21_Click(sender As Object, e As EventArgs) Handles Button21.Click 'Limpiar Viaje
        TextBox8.Text = ""
        TextBox10.Text = ""
        TextBox11.Text = ""
    End Sub

    

    'Metodos Consultas

    Private Sub Button22_Click(sender As Object, e As EventArgs) Handles Button22.Click
        ListBoxProdConsul.Items.Clear()
        Dim fi As Date = DateTimePicker3.Value
        Dim ff As Date = DateTimePicker4.Value
        Dim num As Integer
        Dim viajeAux As Viaje = New Viaje

        If ListBoxTrenConsul.SelectedItem = "" Then
            MessageBox.Show("No hay ningún tren seleccionado")
        Else
            viajeAux.Tren.Matricula = ListBoxTrenConsul.SelectedItem
            Try
                viajeAux.Gestor.numViajes(fi, ff, num, viajeAux)
                Label19.Text = num
                For Each prodAux As Producto In viajeAux.Productos
                    prodAux.leerProducto(prodAux.Id)
                    ListBoxProdConsul.Items.Add(prodAux.Descripcion)
                Next
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub Button23_Click(sender As Object, e As EventArgs) Handles Button23.Click
        ListBoxTTrenCon.Items.Clear()
        Dim fi As Date = DateTimePicker5.Value
        Dim ff As Date = DateTimePicker6.Value
        Dim tipo, tipoAux As TipoTren
        tipo = New TipoTren
        Try
            viaje.trenesFecha(fi, ff, tipo)
            For Each tipoAux In tipo.Gestor.Lista
                tipoAux.leerTipo(tipoAux.TipoTren)
                ListBoxTTrenCon.Items.Add(tipoAux.Descripcion)
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button24_Click(sender As Object, e As EventArgs) Handles Button24.Click
        ListBoxProdC.Items.Clear()
        Dim fi As Date = DateTimePicker7.Value
        Dim ff As Date = DateTimePicker8.Value
        Dim prod, prodAux As Producto
        prod = New Producto
        Try
            viaje.productosFecha(fi, ff, prod)
            For Each prodAux In prod.Gestor.Lista
                prodAux.leerProducto(prodAux.Id)
                ListBoxProdC.Items.Add(prodAux.Descripcion)
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button25_Click(sender As Object, e As EventArgs) Handles Button25.Click
        ListBoxPTotal.Items.Clear()
        Dim total, comparador As Double
        Dim toneladas, viajeC, viajeSol As Integer
        toneladas = 0
        viajeC = 0
        total = 0.0
        For Each viajeAux As Viaje In viaje.Gestor.Lista
            viajeC += 1
            toneladas = 0
            comparador = 0.0
            For Each prodAux As Producto In viajeAux.Productos
                toneladas += 1
                For Each coti As Cotizacion In prodAux.Cotizaciones
                    If coti.Fecha = viajeAux.Fecha Then ' comparador va almacenando el precio de cada viaje
                        comparador = comparador + coti.Euros * viajeAux.Toneladas(toneladas)
                    End If
                Next
            Next
            If comparador > total Then
                total = comparador
                viajeSol = viajeC
            End If
        Next
        Label27.Text = viaje.Gestor.Lista(viajeSol).Fecha
        Label29.Text = viaje.Gestor.Lista(viajeSol).Tren.Matricula
        Label35.Text = total & " €"
        For Each prodAux In viaje.Gestor.Lista(viajeSol).Productos
            ListBoxPTotal.Items.Add(prodAux.Descripcion)
        Next


        MessageBox.Show(total)
    End Sub

    Private Sub ListBoxPTotal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxPTotal.SelectedIndexChanged
        Dim descr As String = ListBoxPTotal.SelectedItem
        Dim matr As String = Label29.Text
        Dim fecha As Date = Label27.Text
        Dim viajeAux As Viaje = New Viaje
        Dim prodAux As Producto = New Producto
        Dim i As Integer = 0
        Dim pos, id, tonel As Integer
        Dim cotiz As Double
        For Each prodAux In producto.Gestor.Lista
            If prodAux.Descripcion = descr Then
                id = prodAux.Id
            End If
        Next
        Try
            prodAux.leerProducto(id)
            For Each coti As Cotizacion In prodAux.Cotizaciones
                If coti.Fecha = fecha Then
                    Label37.Text = coti.Euros & " €"
                    cotiz = coti.Euros
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Try
            viajeAux.leerFechaTren(fecha, matr)
            For Each prodAux In viajeAux.Productos
                i = i + 1
                If prodAux.Id = id Then
                    pos = i
                    Label36.Text = viajeAux.Toneladas(pos)
                    tonel = viajeAux.Toneladas(pos)
                End If
            Next
            Label38.Text = tonel * cotiz
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

   
End Class
