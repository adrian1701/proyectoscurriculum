package practicaSistemas;

public class Sucesor {
	protected Estado estado;
	protected String accion;
	protected boolean cambia;
	protected float desnivel;
	
	public Sucesor(Estado estado, String accion, float desnivel) {
		super();
		this.estado = estado;
		this.accion = accion;
		this.desnivel = desnivel;
		this.cambia=false;
	}

	public Sucesor(Estado estado, String accion) {
		super();
		this.estado = estado;
		this.accion = accion;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public boolean isCambia() {
		return cambia;
	}

	public void setCambia(boolean cambia) {
		this.cambia = cambia;
	}

	public float getDesnivel() {
		return desnivel;
	}

	public void setDesnivel(float desnivel) {
		this.desnivel = desnivel;
	}
	
	
	
	
	
	
	
	
	
}
