#!/usr/bin/python -u
# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
import Ice
Ice.loadSlice('-I %s container.ice' % Ice.getSliceDir())
import Services


class ContainerI(Services.Container):
    def __init__(self):
        self.proxies = dict()

    def link(self, key, proxy, current=None):
        if key in self.proxies:
            raise Services.AlreadyExists(key)

        print("link: {0} -> {1}".format(key, proxy))
        self.proxies[key] = proxy

    def unlink(self, key, current=None):
        if not key in self.proxies:
            raise Services.NoSuchKey(key)

        print("unlink: {0}".format(key))
        del self.proxies[key]

    def list(self, current=None):
		return self.proxies


class Server(Ice.Application):
    def run(self, argv):
        broker = self.communicator()

		# creamos el proxy del Contenedor de robots controller
        servant = ContainerI()
		adapter = broker.createObjectAdapterWithEndpoints("ContainerAdapter","default")
        proxy = adapter.addWithUUID(servant)
		
       
		#creamos el proxy principal  que almacenara el contenedor de robots controller y el proxy de la factoria
        servantP = ContainerI()
		adapterP = broker.createObjectAdapterWithEndpoints("ContainerAdapterP","default")
        proxyP = adapter.addWithUUID(servantP)

		
		#almacenamos el proxy de robots controller en el contenedor principal
		nombre = "proxyContainerRC"
		container = Services.ContainerPrx.checkedCast(proxyP)
		container.link(nombre,proxy)
		
		# se muestra el proxy del contenedor general para poder conectarlo con el 
		# programa principal y la factoria

		print("")
		print("proxy del container")
		print(proxyP)
		print("")
		
        adapter.activate()
		adapterP.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()



if __name__ == '__main__':
    sys.exit(Server().main(sys.argv))
