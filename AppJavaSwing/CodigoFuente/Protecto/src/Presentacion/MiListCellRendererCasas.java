package Presentacion;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;

class MiListCellRendererCasas extends DefaultListCellRenderer {
protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
ImageIcon iconoCa1;
ImageIcon iconoCa2;
ImageIcon iconoCa3;


public MiListCellRendererCasas () {
iconoCa1 = new ImageIcon(MiListCellRendererCasas.class.getResource("Casa1.jpg"));
iconoCa2 = new ImageIcon(MiListCellRendererCasas.class.getResource("Casa2.jpg"));
iconoCa3 = new ImageIcon(MiListCellRendererCasas.class.getResource("Casa3.jpg"));
}
public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasFocus)
{
JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, hasFocus);
if (list.getModel().getElementAt(index) == "Casa1") {
renderer.setIcon(iconoCa1);
} else
if (list.getModel().getElementAt(index) == "Casa2"){
renderer.setIcon(iconoCa2);
}else
renderer.setIcon(iconoCa3);
return renderer;
}
}