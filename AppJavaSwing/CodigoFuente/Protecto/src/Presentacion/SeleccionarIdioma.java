package Presentacion;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.ImageIcon;
import javax.swing.border.TitledBorder;

public class SeleccionarIdioma extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JLabel lblSeleccionaUnIdioma;
	private JRadioButton rdbtnEspaol;
	private JRadioButton rdbtnIngles;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			SeleccionarIdioma dialog = new SeleccionarIdioma();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public SeleccionarIdioma() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new TitledBorder(null, "Selecci\u00F3n de idioma", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			lblSeleccionaUnIdioma = new JLabel("Selecciona un idioma");
			lblSeleccionaUnIdioma.setBounds(46, 73, 135, 58);
			contentPanel.add(lblSeleccionaUnIdioma);
		}
		{
			rdbtnEspaol = new JRadioButton("Español");
			rdbtnEspaol.setIcon(new ImageIcon(SeleccionarIdioma.class.getResource("/Presentacion/banderaEsp.gif")));
			rdbtnEspaol.setBounds(194, 73, 109, 23);
			contentPanel.add(rdbtnEspaol);
		}
		{
			rdbtnIngles = new JRadioButton("Ingles");
			rdbtnIngles.setIcon(new ImageIcon(SeleccionarIdioma.class.getResource("/Presentacion/banderaIng.gif")));
			rdbtnIngles.setBounds(194, 108, 109, 23);
			contentPanel.add(rdbtnIngles);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						if(rdbtnIngles.isSelected()){							
						Messages.setIdioma("ing");
						}
						Entrar ventana = new Entrar();
						ventana.getFrame().setVisible(true);
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
