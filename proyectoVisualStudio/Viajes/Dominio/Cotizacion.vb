﻿Public Class Cotizacion
    Private _producto As Producto
    Private _fecha As Date
    Private eurosTonelada As Double
    Private mGestorC As GestorCotizacion
    Property Producto As Producto
        Get
            Return _producto
        End Get
        Set(ByVal value As Producto)
            _producto = value
        End Set
    End Property
    Property Fecha As Date
        Get
            Return _fecha
        End Get
        Set(ByVal value As Date)
            _fecha = value
        End Set
    End Property
    Property Euros As Double
        Get
            Return eurosTonelada
        End Get
        Set(ByVal value As Double)
            eurosTonelada = value
        End Set
    End Property
    Property Gestor As GestorCotizacion
        Get
            Return mGestorC
        End Get
        Set(value As GestorCotizacion)

        End Set
    End Property
    Sub New(ByVal fecha As Date, ByVal euros As Double)
        _producto = New Producto
        Me.Fecha = fecha
        Me.Euros = euros
        Me.Gestor = New GestorCotizacion
    End Sub
    Sub New(ByRef prod As Producto, ByVal fecha As Date, ByVal euros As Double)
        Me.Producto = prod
        Me.Fecha = fecha
        Me.Euros = euros
        mGestorC = New GestorCotizacion
    End Sub
    Sub New()
        mGestorC = New GestorCotizacion
        _producto = New Producto
    End Sub
    Public Sub leerTodo()
        Me.Gestor.leerTodos()
    End Sub
    Public Sub leerCotizacion(ByRef prod As Producto)
        Me.Producto = prod
        Me.Gestor.leerCotizacion(Me)
    End Sub
    Public Sub leerCotizacionFecha(ByRef prod As Producto, ByVal fecha As Date)
        Me.Fecha = fecha
        Me.Producto = prod
        Me.Gestor.leerCotizacionFecha(Me)
    End Sub
    Public Sub añadir()
        Me.Gestor.añadir(Me)
    End Sub
    Public Sub modificar()
        Me.Gestor.modificar(Me)
    End Sub
    Public Sub eliminar()
        Me.Gestor.eliminar(Me)
    End Sub
End Class
