package Presentacion;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.border.TitledBorder;
import javax.swing.JButton;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import java.awt.Dimension;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JCheckBox;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.awt.event.ActionEvent;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JRadioButton;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import javax.swing.event.ListSelectionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import javax.swing.JTable;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JSplitPane;

public class MenuPrincipal extends JFrame {

	protected static final Component framer = null;
	private JPanel contentPane;
	private JTabbedPane tabbedPane;
	private JPanel panelMascotas;
	private JPanel panelCasas;
	private JPanel panelVoluntarios;
	private JPanel panelBusqueda;
	private JPanel panelAyuda;
	private JToolBar toolBar;
	private JButton btnAadirPerro;
	private JButton btnBorrarPerro;
	private JButton btnModificarPerro;
	private JScrollPane scrollPane;
	private JPanel panel;
	private JList listPerros;
	private JLabel lblNombrePerro;
	private JLabel lblRaza;
	private JLabel lblPeso;
	private JLabel lblEdadPerro;
	private JLabel lblTamao;
	private JTextField textNombrePerro;
	private JSpinner spinnerEdadPerro;
	private JSpinner spinner;
	private JComboBox comboBoxTamano;
	private JLabel lblDescripcion;
	private JTextArea textAreaPerros;
	private JLabel labelFotoPerro;
	private JButton btnFotoPerro;
	private JCheckBox chckbxVacunado;
	private JCheckBox chckbxEsterelizado;
	private JCheckBox chckbxSociable;
	private JCheckBox chckbxCachorro;
	private JCheckBox chckbxChip;
	private JTextField textFieldRaza;
	private JLabel lblEstado;
	private JComboBox comboBoxEstado;
	private JButton btnSalirPerros;
	private JToolBar toolBarVoluntarios;
	private JButton btnAadirVoluntario;
	private JButton btnBorrarVoluntario;
	private JButton btnModificarVoluntario;
	private JButton btnSalirVoluntarios;
	private JScrollPane scrollPaneVoluntarios;
	private JList listVoluntarios;
	private JPanel panelVolunt;
	private JLabel lblNombreVoluntario;
	private JLabel lblApellidosVoluntario;
	private JLabel lblEdadVoluntario;
	private JLabel lblDni;
	private JLabel lblEmail;
	private JTextField textFieldNombreVoluntario;
	private JTextField textFieldApellVolunt;
	private JSpinner spinner_1EdadVolunt;
	private JTextField textFieldDNI;
	private JTextField textFieldEmail;
	private JLabel lblTelefono;
	private JLabel lblDomicilio;
	private JTextField textFieldDomicilio;
	private JLabel lblN;
	private JSpinner spinner_1Numero;
	private JLabel lblHorario;
	private JTextField textFieldTelefono;
	private JComboBox comboBoxInicio;
	private JComboBox comboBoxFin;
	private JLabel labelSeparador;
	private JButton btnFotoVolunt;
	private JLabel labelFotoVolunt;
	private JToolBar toolBarBusqueda;
	private JButton buttonCargarMapa;
	private JButton btnSalida;
	private JButton btnReunion;
	private JButton buttonPerroVsisto;
	private JButton buttonGrupoBusq;
	private JButton buttonComisaria;
	private JButton buttonAnotacion;
	private JScrollPane scrollPaneMapa;
	
	private MiAreaDibujo miAreaDibujo;
	
	private ImageIcon imagen;
	
	int modo = -1;
	private final int START = 1;
	private final int ENCUENTRO = 2;
	private final int PERRO = 3;
	private final int GRUPO = 4;
	private final int COMISARIA = 5;
	private final int TEXTO = 6;
	private final int AREA = 7;


	
	
	private Toolkit toolkit;
	
	private Image imagStart;
	private Image imagEncuentro;
	private Image imagPerro;
	private Image imagGrupo;
	private Image imagPolicia;


	private JTextField txtTexto = new JTextField();

	private int x, y;
	private JButton btnAreaMapa;
	private JButton buttonDeleteMapa;
	private JPanel panelPerdidos;
	private JToolBar toolBar_Perdidos;
	private JButton btnAadirPerdido;
	private JButton btnBorrarPerdidos;
	private JScrollPane scrollPane_1Perdidos;
	private JTable tablePerdidos;
	private JButton btnModificarPerdido;
	private JButton btnSalirPerdido;
	private JToolBar toolBar_1Casas;
	private JButton btnAadirCasa;
	private JButton btnBorrarCasa;
	private JButton btnModificarCasa;
	private JScrollPane scrollPane_1Casa;
	private JList listCasa;
	private JPanel panel_1Casa;
	private JLabel lblPropietario;
	private JLabel lblDireccion;
	private JTextField textFieldPropietario;
	private JTextField textField;
	private JLabel lblNumeroCasa;
	private JSpinner spinner_1Casa;
	private JCheckBox chckbxAdaptadaParaAnimales;
	private JCheckBox chckbxInspeccionada;
	private JCheckBox chckbxCasaConNios;
	private JButton btnFotoCasas;
	private JScrollPane scrollPane_1FotoCasa;
	private JLabel labelFotoGrandeCasa;
	private JLabel lblAnimalesAdoptados;
	private JSpinner spinner_1Adoptados;
	private JPanel panelDonativos;
	private JButton btnSalir;
	private JPanel panel_Dona;
	private JLabel lblContruccinDeCasetas;
	private JSpinner spinner_1Casetas;
	private JLabel lblS;
	private JLabel lblReparacinDePiscina;
	private JSpinner spinner_1;
	private JLabel label;
	private JLabel lblCreacinDePeluquera;
	private JSpinner spinner_2;
	private JLabel label_1;
	private JLabel lblNuevasInstalaciones;
	private JSpinner spinner_3;
	private JLabel label_2;
	private JLabel lblNuevaClinicaVeterinaria;
	private JSpinner spinner_4;
	private JLabel label_3;
	private JLabel lblFormasDePago;
	private JButton btnPaypal;
	private JButton buttonTarjeta;
	private JButton btnPagar;
	private JLabel lblTotal;
	private JLabel label_4;
	private JSpinner spinner_Total;
	private JPanel panel_Padrinos;
	private JToolBar toolBar_1Padrino;
	private JButton btnAadirPadrino;
	private JPanel panel_PadrinInfo;
	private JLabel lblNombreDelPadrino;
	private JTextField textField_1;
	private JLabel lblDomicilio_1;
	private JTextField textField_2;
	private JLabel lblNumero;
	private JSpinner spinner_5;
	private JLabel lblTelefono_1;
	private JTextField textField_3;
	private JLabel lblNumeroDeCuenta;
	private JTextField textField_4;
	private JLabel lblPint;
	private JTextField textField_5;
	private JLabel lblFormaDePago;
	private JComboBox comboBox_1;
	private JLabel lblAportacinMensual;
	private JSpinner spinner_6;
	private JLabel lblNombreDePerro;
	private JTextField textField_6;
	private JButton btnFoto;
	private JButton btnBorrarPadrino;
	private JButton btnModificarPadrino;
	private JButton btnSalir_1;
	private JTextArea txtrAplicacinCreadaPor;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuPrincipal frame = new MenuPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 801, 596);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		
		setContentPane(contentPane);
		{
			tabbedPane = new JTabbedPane(JTabbedPane.LEFT);
			tabbedPane.setBorder(new TitledBorder(null, Messages.getString("MenuPrincipal.0"), TitledBorder.LEADING, TitledBorder.TOP, null, null)); //$NON-NLS-1$
			contentPane.add(tabbedPane, BorderLayout.CENTER);
			{
				panelMascotas = new JPanel();
				tabbedPane.addTab(Messages.getString("MenuPrincipal.1"), new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/Perro.jpg")), panelMascotas, null); //$NON-NLS-1$ //$NON-NLS-2$
				panelMascotas.setLayout(new BorderLayout(0, 0));
				{
					toolBar = new JToolBar();
					panelMascotas.add(toolBar, BorderLayout.SOUTH);
					{
						btnAadirPerro = new JButton(Messages.getString("MenuPrincipal.3")); //$NON-NLS-1$
						btnAadirPerro.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								textAreaPerros.setText(""); //$NON-NLS-1$
								DefaultListModel modeloLista= (DefaultListModel) listPerros.getModel();
								int indice = modeloLista.getSize();
								
								modeloLista.addElement(Messages.getString("MenuPrincipal.5")); //$NON-NLS-1$
								listPerros.setSelectedIndex(indice);
								listPerros.ensureIndexIsVisible(indice);
							}
						});
						
						btnAadirPerro.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/ic_menu_btn_add.png"))); //$NON-NLS-1$
						toolBar.add(btnAadirPerro);
					}
					{
						btnBorrarPerro = new JButton(Messages.getString("MenuPrincipal.7")); //$NON-NLS-1$
						btnBorrarPerro.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								
								int valor= JOptionPane.showConfirmDialog(framer	, Messages.getString("MenuPrincipal.8")); //$NON-NLS-1$
								if( valor == 0){
								textAreaPerros.setText(""); //$NON-NLS-1$
								DefaultListModel modeloLista= (DefaultListModel) listPerros.getModel();
								int indice =listPerros.getSelectedIndex();
								modeloLista.remove(indice);
								
								}
							}
						});
						btnBorrarPerro.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/ic_menu_delete.png"))); //$NON-NLS-1$
						toolBar.add(btnBorrarPerro);
					}
					{
						btnModificarPerro = new JButton("Modificar"); //$NON-NLS-1$
						btnModificarPerro.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/Modificar.gif"))); //$NON-NLS-1$
						toolBar.add(btnModificarPerro);
					}
					{
						btnSalirPerros = new JButton(Messages.getString("MenuPrincipal.13")); //$NON-NLS-1$
						btnSalirPerros.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								int valor= JOptionPane.showConfirmDialog(framer	, Messages.getString("MenuPrincipal.14")); //$NON-NLS-1$
								if( valor == 0){
									System.exit(0);
								}
							}
						});
						btnSalirPerros.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/exit.png"))); //$NON-NLS-1$
						toolBar.add(btnSalirPerros);
					}
				}
				{
					scrollPane = new JScrollPane();
					scrollPane.setPreferredSize(new Dimension(150, 150));
					panelMascotas.add(scrollPane, BorderLayout.NORTH);
					{
						listPerros = new JList();
						
						DefaultListModel modeloLista = new DefaultListModel();
						listPerros.setModel(modeloLista);
						//Añadimos dos elementos de prueba a la lista
						modeloLista.addElement("Perry"); //$NON-NLS-1$
						modeloLista.addElement("Guagua"); //$NON-NLS-1$
						modeloLista.addElement("Juan"); //$NON-NLS-1$
						modeloLista.addElement("Juan"); //$NON-NLS-1$

						listPerros.setCellRenderer(new MiListCellRendererPerros());
						scrollPane.setViewportView(listPerros);
					}
				}
				{
					panel = new JPanel();
					panelMascotas.add(panel, BorderLayout.CENTER);
					panel.setLayout(null);
					{
						lblNombrePerro = new JLabel(Messages.getString("MenuPrincipal.20")); //$NON-NLS-1$
						lblNombrePerro.setBounds(20, 34, 46, 14);
						panel.add(lblNombrePerro);
					}
					{
						lblRaza = new JLabel(Messages.getString("MenuPrincipal.21")); //$NON-NLS-1$
						lblRaza.setBounds(20, 118, 46, 14);
						panel.add(lblRaza);
					}
					{
						lblPeso = new JLabel(Messages.getString("MenuPrincipal.22")); //$NON-NLS-1$
						lblPeso.setBounds(20, 93, 46, 14);
						panel.add(lblPeso);
					}
					{
						lblEdadPerro = new JLabel(Messages.getString("MenuPrincipal.23")); //$NON-NLS-1$
						lblEdadPerro.setBounds(20, 65, 46, 14);
						panel.add(lblEdadPerro);
					}
					{
						lblTamao = new JLabel(Messages.getString("MenuPrincipal.24")); //$NON-NLS-1$
						lblTamao.setBounds(20, 198, 46, 14);
						panel.add(lblTamao);
					}
					{
						textNombrePerro = new JTextField();
						textNombrePerro.setBounds(67, 31, 112, 20);
						panel.add(textNombrePerro);
						textNombrePerro.setColumns(10);
					}
					{
						spinnerEdadPerro = new JSpinner();
						spinnerEdadPerro.setBounds(67, 62, 69, 20);
						panel.add(spinnerEdadPerro);
					}
					{
						spinner = new JSpinner();
						spinner.setBounds(67, 87, 71, 20);
						panel.add(spinner);
					}
					{
						comboBoxTamano = new JComboBox();
						comboBoxTamano.setModel(new DefaultComboBoxModel(new String[] {Messages.getString("MenuPrincipal.25"), Messages.getString("MenuPrincipal.26"), Messages.getString("MenuPrincipal.27")})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						comboBoxTamano.setBounds(67, 195, 112, 20);
						panel.add(comboBoxTamano);
					}
					{
						lblDescripcion = new JLabel(Messages.getString("MenuPrincipal.28")); //$NON-NLS-1$
						lblDescripcion.setBounds(10, 236, 89, 42);
						panel.add(lblDescripcion);
					}
					{
						textAreaPerros = new JTextArea();
						textAreaPerros.setBounds(91, 236, 486, 53);
						panel.add(textAreaPerros);
					}
					{
						labelFotoPerro = new JLabel(""); //$NON-NLS-1$
						labelFotoPerro.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/PerroInicio.jpg"))); //$NON-NLS-1$
						labelFotoPerro.setBounds(260, 30, 156, 139);
						panel.add(labelFotoPerro);
					}
					{
						btnFotoPerro = new JButton(Messages.getString("MenuPrincipal.31")); //$NON-NLS-1$
						btnFotoPerro.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/cargarFoto.png"))); //$NON-NLS-1$
						btnFotoPerro.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								JFileChooser fcAbrir = new JFileChooser();
								fcAbrir.setFileFilter(new ImagenFiltro());
								int valorDevuelto = fcAbrir.showOpenDialog(framer);
								//Recoger el nombre del fichero seleccionado por el usuario
								if (valorDevuelto == JFileChooser.APPROVE_OPTION) {
								File file = fcAbrir.getSelectedFile();
								
								
								labelFotoPerro.setIcon(new ImageIcon(file.getAbsolutePath()));
								}
								
							}
						});
						btnFotoPerro.setBounds(258, 180, 147, 37);
						panel.add(btnFotoPerro);
					}
					{
						chckbxVacunado = new JCheckBox(Messages.getString("MenuPrincipal.33")); //$NON-NLS-1$
						chckbxVacunado.setBounds(437, 47, 97, 23);
						panel.add(chckbxVacunado);
					}
					{
						chckbxEsterelizado = new JCheckBox(Messages.getString("MenuPrincipal.34")); //$NON-NLS-1$
						chckbxEsterelizado.setBounds(437, 77, 97, 23);
						panel.add(chckbxEsterelizado);
					}
					{
						chckbxSociable = new JCheckBox(Messages.getString("MenuPrincipal.35")); //$NON-NLS-1$
						chckbxSociable.setBounds(437, 103, 97, 23);
						panel.add(chckbxSociable);
					}
					{
						chckbxCachorro = new JCheckBox(Messages.getString("MenuPrincipal.36")); //$NON-NLS-1$
						chckbxCachorro.setBounds(437, 129, 97, 23);
						panel.add(chckbxCachorro);
					}
					{
						chckbxChip = new JCheckBox(Messages.getString("MenuPrincipal.37")); //$NON-NLS-1$
						chckbxChip.setBounds(437, 159, 97, 23);
						panel.add(chckbxChip);
					}
					{
						textFieldRaza = new JTextField();
						textFieldRaza.setBounds(67, 115, 112, 20);
						panel.add(textFieldRaza);
						textFieldRaza.setColumns(10);
					}
					{
						lblEstado = new JLabel(Messages.getString("MenuPrincipal.38")); //$NON-NLS-1$
						lblEstado.setBounds(20, 155, 46, 14);
						panel.add(lblEstado);
					}
					{
						comboBoxEstado = new JComboBox();
						comboBoxEstado.setModel(new DefaultComboBoxModel(new String[] {Messages.getString("MenuPrincipal.39"), Messages.getString("MenuPrincipal.40"), Messages.getString("MenuPrincipal.41"), Messages.getString("MenuPrincipal.42")})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
						comboBoxEstado.setBounds(67, 149, 112, 20);
						panel.add(comboBoxEstado);
					}
				}
				{
					{
						DefaultListModel modeloLista = new DefaultListModel();
						modeloLista.addElement("galgo"); //$NON-NLS-1$
						modeloLista.addElement("labrador"); //$NON-NLS-1$
					}
				}
			}
			{
				panelCasas = new JPanel();
				tabbedPane.addTab(Messages.getString("MenuPrincipal.45"), new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/Casa.png")), panelCasas, null); //$NON-NLS-1$ //$NON-NLS-2$
				panelCasas.setLayout(new BorderLayout(0, 0));
				{
					toolBar_1Casas = new JToolBar();
					panelCasas.add(toolBar_1Casas, BorderLayout.SOUTH);
					{
						btnAadirCasa = new JButton(Messages.getString("MenuPrincipal.47")); //$NON-NLS-1$
						btnAadirCasa.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								
								DefaultListModel modeloListaCasa= (DefaultListModel) listCasa.getModel();
								int indice = modeloListaCasa.getSize();
								modeloListaCasa.addElement("Casa3"); //$NON-NLS-1$
								listCasa.setSelectedIndex(indice);
								listCasa.ensureIndexIsVisible(indice);
							}
						});
						btnAadirCasa.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/ic_menu_btn_add.png"))); //$NON-NLS-1$
						toolBar_1Casas.add(btnAadirCasa);
					}
					{
						btnBorrarCasa = new JButton(Messages.getString("MenuPrincipal.50")); //$NON-NLS-1$
						btnBorrarCasa.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								
								int valor= JOptionPane.showConfirmDialog(framer	, Messages.getString("MenuPrincipal.51")); //$NON-NLS-1$
								if( valor == 0){
								DefaultListModel modeloListaCasa= (DefaultListModel) listCasa.getModel();

								int indice = listCasa.getSelectedIndex();
								modeloListaCasa.remove(indice);
								}
							}
						});
						btnBorrarCasa.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/ic_menu_delete.png"))); //$NON-NLS-1$
						toolBar_1Casas.add(btnBorrarCasa);
					}
					{
						btnModificarCasa = new JButton(Messages.getString("MenuPrincipal.53")); //$NON-NLS-1$
						btnModificarCasa.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/Modificar.gif"))); //$NON-NLS-1$
						toolBar_1Casas.add(btnModificarCasa);
					}
					{
						btnSalir = new JButton(Messages.getString("MenuPrincipal.55")); //$NON-NLS-1$
						btnSalir.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								int valor= JOptionPane.showConfirmDialog(framer	, Messages.getString("MenuPrincipal.56")); //$NON-NLS-1$
								if( valor == 0){
									System.exit(0);
								}
							}
						});
						btnSalir.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/exit.png"))); //$NON-NLS-1$
						toolBar_1Casas.add(btnSalir);
					}
				}
				{
					scrollPane_1Casa = new JScrollPane();
					scrollPane_1Casa.setPreferredSize(new Dimension(120, 130));
					panelCasas.add(scrollPane_1Casa, BorderLayout.NORTH);
					{
						listCasa = new JList();
						
						listCasa.setCellRenderer(new MiListCellRendererCasas());
						DefaultListModel modeloListaCasa = new DefaultListModel();
						listCasa.setModel(modeloListaCasa);
						//Añadimos dos elementos de prueba a la lista
						modeloListaCasa.addElement("Casa1"); //$NON-NLS-1$
						modeloListaCasa.addElement("Casa2"); //$NON-NLS-1$
						
						scrollPane_1Casa.setViewportView(listCasa);
					}
				}
				{
					panel_1Casa = new JPanel();
					panelCasas.add(panel_1Casa, BorderLayout.CENTER);
					panel_1Casa.setLayout(null);
					{
						lblPropietario = new JLabel(Messages.getString("MenuPrincipal.60")); //$NON-NLS-1$
						lblPropietario.setBounds(28, 63, 90, 14);
						panel_1Casa.add(lblPropietario);
					}
					{
						lblDireccion = new JLabel(Messages.getString("MenuPrincipal.61")); //$NON-NLS-1$
						lblDireccion.setBounds(28, 91, 58, 14);
						panel_1Casa.add(lblDireccion);
					}
					{
						textFieldPropietario = new JTextField();
						textFieldPropietario.setBounds(96, 60, 111, 20);
						panel_1Casa.add(textFieldPropietario);
						textFieldPropietario.setColumns(10);
					}
					{
						textField = new JTextField();
						textField.setBounds(96, 88, 111, 20);
						panel_1Casa.add(textField);
						textField.setColumns(10);
					}
					{
						lblNumeroCasa = new JLabel(Messages.getString("MenuPrincipal.62")); //$NON-NLS-1$
						lblNumeroCasa.setBounds(28, 131, 108, 14);
						panel_1Casa.add(lblNumeroCasa);
					}
					{
						spinner_1Casa = new JSpinner();
						spinner_1Casa.setBounds(135, 128, 40, 20);
						panel_1Casa.add(spinner_1Casa);
					}
					{
						chckbxAdaptadaParaAnimales = new JCheckBox(Messages.getString("MenuPrincipal.63")); //$NON-NLS-1$
						chckbxAdaptadaParaAnimales.setBounds(28, 206, 179, 23);
						panel_1Casa.add(chckbxAdaptadaParaAnimales);
					}
					{
						chckbxInspeccionada = new JCheckBox(Messages.getString("MenuPrincipal.64")); //$NON-NLS-1$
						chckbxInspeccionada.setBounds(28, 244, 168, 23);
						panel_1Casa.add(chckbxInspeccionada);
					}
					{
						chckbxCasaConNios = new JCheckBox(Messages.getString("MenuPrincipal.65")); //$NON-NLS-1$
						chckbxCasaConNios.setBounds(28, 283, 147, 23);
						panel_1Casa.add(chckbxCasaConNios);
					}
					{
						btnFotoCasas = new JButton(Messages.getString("MenuPrincipal.66")); //$NON-NLS-1$
						btnFotoCasas.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/cargarFoto.png"))); //$NON-NLS-1$
						btnFotoCasas.setBounds(348, 244, 179, 45);
						panel_1Casa.add(btnFotoCasas);
					}
					{
						scrollPane_1FotoCasa = new JScrollPane();
						scrollPane_1FotoCasa.setBounds(260, 40, 337, 175);
						panel_1Casa.add(scrollPane_1FotoCasa);
						{
							labelFotoGrandeCasa = new JLabel(""); //$NON-NLS-1$
							scrollPane_1FotoCasa.setViewportView(labelFotoGrandeCasa);
						}
					}
					{
						lblAnimalesAdoptados = new JLabel(Messages.getString("MenuPrincipal.69")); //$NON-NLS-1$
						lblAnimalesAdoptados.setBounds(28, 172, 117, 14);
						panel_1Casa.add(lblAnimalesAdoptados);
					}
					{
						spinner_1Adoptados = new JSpinner();
						spinner_1Adoptados.setBounds(156, 169, 37, 20);
						panel_1Casa.add(spinner_1Adoptados);
					}
				}
			}
			{
				panelVoluntarios = new JPanel();
				tabbedPane.addTab(Messages.getString("MenuPrincipal.70"), new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/personaS.jpg")), panelVoluntarios, null); //$NON-NLS-1$ //$NON-NLS-2$
				panelVoluntarios.setLayout(new BorderLayout(0, 0));
				{
					toolBarVoluntarios = new JToolBar();
					panelVoluntarios.add(toolBarVoluntarios, BorderLayout.SOUTH);
					{
						btnAadirVoluntario = new JButton(Messages.getString("MenuPrincipal.72")); //$NON-NLS-1$
						btnAadirVoluntario.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								DefaultListModel modeloListaVoluntarios= (DefaultListModel) listVoluntarios.getModel();
								int indice = modeloListaVoluntarios.getSize();
								
								modeloListaVoluntarios.addElement("Cesar"); //$NON-NLS-1$
								listVoluntarios.setSelectedIndex(indice);
								listVoluntarios.ensureIndexIsVisible(indice);
							}
						});
						btnAadirVoluntario.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/ic_menu_btn_add.png"))); //$NON-NLS-1$
						toolBarVoluntarios.add(btnAadirVoluntario);
					}
					{
						btnBorrarVoluntario = new JButton(Messages.getString("MenuPrincipal.75")); //$NON-NLS-1$
						btnBorrarVoluntario.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								int valor= JOptionPane.showConfirmDialog(framer	, Messages.getString("MenuPrincipal.76")); //$NON-NLS-1$
								if( valor == 0){
									DefaultListModel modeloListaVoluntarios= (DefaultListModel) listVoluntarios.getModel();
									int indice = listVoluntarios.getSelectedIndex();
									modeloListaVoluntarios.remove(indice);
								}
							}
						});
						btnBorrarVoluntario.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/ic_menu_delete.png"))); //$NON-NLS-1$
						toolBarVoluntarios.add(btnBorrarVoluntario);
					}
					{
						btnModificarVoluntario = new JButton(Messages.getString("MenuPrincipal.78")); //$NON-NLS-1$
						btnModificarVoluntario.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/Modificar.gif"))); //$NON-NLS-1$
						toolBarVoluntarios.add(btnModificarVoluntario);
					}
					{
						btnSalirVoluntarios = new JButton(Messages.getString("MenuPrincipal.80")); //$NON-NLS-1$
						btnSalirVoluntarios.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								int valor= JOptionPane.showConfirmDialog(framer	, Messages.getString("MenuPrincipal.81")); //$NON-NLS-1$
								if( valor == 0){
									System.exit(0);
								}
							}
						});
						btnSalirVoluntarios.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/exit.png"))); //$NON-NLS-1$
						toolBarVoluntarios.add(btnSalirVoluntarios);
					}
				}
				{
					scrollPaneVoluntarios = new JScrollPane();
					scrollPaneVoluntarios.setPreferredSize(new Dimension(150, 150));
					panelVoluntarios.add(scrollPaneVoluntarios, BorderLayout.NORTH);
					{
						listVoluntarios = new JList();
						DefaultListModel modeloListaVoluntarios = new DefaultListModel();
						listVoluntarios.setModel(modeloListaVoluntarios);

						modeloListaVoluntarios.addElement("Pedro"); //$NON-NLS-1$
						modeloListaVoluntarios.addElement("Cesar"); //$NON-NLS-1$

						listVoluntarios.setCellRenderer(new MiListCellRendererVoluntarios());
						scrollPaneVoluntarios.setViewportView(listVoluntarios);
					}
				}
				{
					panelVolunt = new JPanel();
					panelVoluntarios.add(panelVolunt, BorderLayout.CENTER);
					panelVolunt.setLayout(null);
					{
						lblNombreVoluntario = new JLabel(Messages.getString("MenuPrincipal.85")); //$NON-NLS-1$
						lblNombreVoluntario.setBounds(23, 62, 74, 14);
						panelVolunt.add(lblNombreVoluntario);
					}
					{
						lblApellidosVoluntario = new JLabel(Messages.getString("MenuPrincipal.86")); //$NON-NLS-1$
						lblApellidosVoluntario.setBounds(22, 98, 75, 14);
						panelVolunt.add(lblApellidosVoluntario);
					}
					{
						lblEdadVoluntario = new JLabel(Messages.getString("MenuPrincipal.87")); //$NON-NLS-1$
						lblEdadVoluntario.setBounds(23, 135, 48, 14);
						panelVolunt.add(lblEdadVoluntario);
					}
					{
						lblDni = new JLabel(Messages.getString("MenuPrincipal.88")); //$NON-NLS-1$
						lblDni.setBounds(23, 171, 46, 14);
						panelVolunt.add(lblDni);
					}
					{
						lblEmail = new JLabel(Messages.getString("MenuPrincipal.89")); //$NON-NLS-1$
						lblEmail.setBounds(23, 214, 46, 14);
						panelVolunt.add(lblEmail);
					}
					{
						textFieldNombreVoluntario = new JTextField();
						textFieldNombreVoluntario.setBounds(79, 59, 97, 20);
						panelVolunt.add(textFieldNombreVoluntario);
						textFieldNombreVoluntario.setColumns(10);
					}
					{
						textFieldApellVolunt = new JTextField();
						textFieldApellVolunt.setBounds(79, 95, 97, 20);
						panelVolunt.add(textFieldApellVolunt);
						textFieldApellVolunt.setColumns(10);
					}
					{
						spinner_1EdadVolunt = new JSpinner();
						spinner_1EdadVolunt.setBounds(77, 132, 95, 20);
						panelVolunt.add(spinner_1EdadVolunt);
					}
					{
						textFieldDNI = new JTextField();
						textFieldDNI.setBounds(77, 168, 97, 20);
						panelVolunt.add(textFieldDNI);
						textFieldDNI.setColumns(10);
					}
					{
						textFieldEmail = new JTextField();
						textFieldEmail.setBounds(77, 211, 99, 20);
						panelVolunt.add(textFieldEmail);
						textFieldEmail.setColumns(10);
					}
					{
						lblTelefono = new JLabel(Messages.getString("MenuPrincipal.90")); //$NON-NLS-1$
						lblTelefono.setBounds(318, 194, 73, 14);
						panelVolunt.add(lblTelefono);
					}
					{
						lblDomicilio = new JLabel(Messages.getString("MenuPrincipal.91")); //$NON-NLS-1$
						lblDomicilio.setBounds(10, 256, 58, 14);
						panelVolunt.add(lblDomicilio);
					}
					{
						textFieldDomicilio = new JTextField();
						textFieldDomicilio.setBounds(79, 253, 97, 20);
						panelVolunt.add(textFieldDomicilio);
						textFieldDomicilio.setColumns(10);
					}
					{
						lblN = new JLabel("Nº"); //$NON-NLS-1$
						lblN.setBounds(194, 256, 46, 14);
						panelVolunt.add(lblN);
					}
					{
						spinner_1Numero = new JSpinner();
						spinner_1Numero.setBounds(215, 253, 46, 20);
						panelVolunt.add(spinner_1Numero);
					}
					{
						lblHorario = new JLabel(Messages.getString("MenuPrincipal.93")); //$NON-NLS-1$
						lblHorario.setBounds(285, 228, 63, 14);
						panelVolunt.add(lblHorario);
					}
					{
						textFieldTelefono = new JTextField();
						textFieldTelefono.setBounds(386, 191, 132, 20);
						panelVolunt.add(textFieldTelefono);
						textFieldTelefono.setColumns(10);
					}
					{
						comboBoxInicio = new JComboBox();
						comboBoxInicio.setModel(new DefaultComboBoxModel(new String[] {"1:00", "2:00", "3:00", "4:00", "5:00", "6:00", "7:00", "8:00", "9:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00", "24:00"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$ //$NON-NLS-20$ //$NON-NLS-21$ //$NON-NLS-22$ //$NON-NLS-23$ //$NON-NLS-24$
						comboBoxInicio.setBounds(358, 225, 82, 20);
						panelVolunt.add(comboBoxInicio);
					}
					{
						comboBoxFin = new JComboBox();
						comboBoxFin.setModel(new DefaultComboBoxModel(new String[] {"1:00", "2:00", "3:00", "4:00", "5:00", "6:00", "7:00", "8:00", "9:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00", "24:00"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$ //$NON-NLS-20$ //$NON-NLS-21$ //$NON-NLS-22$ //$NON-NLS-23$ //$NON-NLS-24$
						comboBoxFin.setBounds(473, 225, 82, 20);
						panelVolunt.add(comboBoxFin);
					}
					{
						labelSeparador = new JLabel("//"); //$NON-NLS-1$
						labelSeparador.setBounds(452, 228, 46, 14);
						panelVolunt.add(labelSeparador);
					}
					{
						btnFotoVolunt = new JButton(Messages.getString("MenuPrincipal.143")); //$NON-NLS-1$
						btnFotoVolunt.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								JFileChooser fcAbrir2 = new JFileChooser();
								fcAbrir2.setFileFilter(new ImagenFiltro());

								int valorDevuelto = fcAbrir2.showOpenDialog(framer);
								if (valorDevuelto == JFileChooser.APPROVE_OPTION) {
								File file2 = fcAbrir2.getSelectedFile();
								labelFotoVolunt.setIcon(new ImageIcon(file2.getAbsolutePath()));

								
								}
							}
						});
						btnFotoVolunt.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/cargarFoto.png"))); //$NON-NLS-1$
						btnFotoVolunt.setBounds(255, 75, 122, 61);
						panelVolunt.add(btnFotoVolunt);
					}
					{
						labelFotoVolunt = new JLabel(""); //$NON-NLS-1$
						labelFotoVolunt.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/Voluntario.jpg"))); //$NON-NLS-1$
						labelFotoVolunt.setBounds(386, 30, 216, 143);
						panelVolunt.add(labelFotoVolunt);
					}
				}
			}
			{
				panelBusqueda = new JPanel();
				tabbedPane.addTab(Messages.getString("MenuPrincipal.147"), new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/ico_mapa_pie.png")), panelBusqueda, null); //$NON-NLS-1$ //$NON-NLS-2$
				panelBusqueda.setLayout(new BorderLayout(0, 0));
				{
					toolkit = Toolkit.getDefaultToolkit();
					imagStart =
					toolkit.getImage(getClass().getClassLoader().getResource("Presentacion/InicioBusqueda.jpg")); //$NON-NLS-1$
					imagEncuentro =
					toolkit.getImage(getClass().getClassLoader().getResource("Presentacion/PuntoEncuentro.png")); //$NON-NLS-1$
					imagPerro =
					toolkit.getImage(getClass().getClassLoader().getResource("Presentacion/perroMapa.jpg")); //$NON-NLS-1$
					imagGrupo =
							toolkit.getImage(getClass().getClassLoader().getResource("Presentacion/personasMapa.jpg")); //$NON-NLS-1$
					
					imagPolicia =
							toolkit.getImage(getClass().getClassLoader().getResource("Presentacion/ComisariaMapa.jpg")); //$NON-NLS-1$
					
					
					toolBarBusqueda = new JToolBar();
					panelBusqueda.add(toolBarBusqueda, BorderLayout.NORTH);
					{
						buttonCargarMapa = new JButton(""); //$NON-NLS-1$
						buttonCargarMapa.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								
								miAreaDibujo.delete();
								miAreaDibujo.setIcon(null);
								JFileChooser fcAbrir = new JFileChooser();
								int valorDevuelto = fcAbrir.showOpenDialog(framer);
								if (valorDevuelto == JFileChooser.APPROVE_OPTION) {
								File file = fcAbrir.getSelectedFile();
								imagen = new ImageIcon(file.getAbsolutePath());
								miAreaDibujo.setIcon(imagen);
								}
							}
						});
						buttonCargarMapa.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/cargarMapa.png"))); //$NON-NLS-1$
						toolBarBusqueda.add(buttonCargarMapa);
					}
					{
						btnSalida = new JButton(""); //$NON-NLS-1$
						btnSalida.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								
								modo = START;
								
							}
						});
						btnSalida.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/InicioBusqueda.jpg"))); //$NON-NLS-1$
						toolBarBusqueda.add(btnSalida);
					}
					{
						btnReunion = new JButton(""); //$NON-NLS-1$
						btnReunion.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								
								modo = ENCUENTRO;
								
							}
						});
						btnReunion.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/PuntoEncuentro.png"))); //$NON-NLS-1$
						toolBarBusqueda.add(btnReunion);
					}
					{
						buttonPerroVsisto = new JButton(""); //$NON-NLS-1$
						buttonPerroVsisto.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								modo = PERRO;
								
							}
						});
						buttonPerroVsisto.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/perroMapa.jpg"))); //$NON-NLS-1$
						toolBarBusqueda.add(buttonPerroVsisto);
					}
					{
						buttonGrupoBusq = new JButton(""); //$NON-NLS-1$
						buttonGrupoBusq.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								modo = GRUPO;
							}
						});
						buttonGrupoBusq.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/personasMapa.jpg"))); //$NON-NLS-1$
						toolBarBusqueda.add(buttonGrupoBusq);
					}
					{
						buttonComisaria = new JButton(""); //$NON-NLS-1$
						buttonComisaria.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								modo = COMISARIA;
							}
						});
						buttonComisaria.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/ComisariaMapa.jpg"))); //$NON-NLS-1$
						toolBarBusqueda.add(buttonComisaria);
					}
					{
						buttonAnotacion = new JButton(""); //$NON-NLS-1$
						buttonAnotacion.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								modo = TEXTO;
							}
						});
						buttonAnotacion.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/aniadirAnotacion.png"))); //$NON-NLS-1$
						toolBarBusqueda.add(buttonAnotacion);
					}
					{
						btnAreaMapa = new JButton(""); //$NON-NLS-1$
						btnAreaMapa.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								modo = AREA;
							}
						});
						btnAreaMapa.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/rectangulo.png"))); //$NON-NLS-1$
						toolBarBusqueda.add(btnAreaMapa);
					}
					{
						buttonDeleteMapa = new JButton(""); //$NON-NLS-1$
						buttonDeleteMapa.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								int valor= JOptionPane.showConfirmDialog(framer	, Messages.getString("MenuPrincipal.171")); //$NON-NLS-1$
								if( valor == 0){
								miAreaDibujo.delete();
								miAreaDibujo.setIcon(null);
							}}
						});
						buttonDeleteMapa.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/ic_menu_delete.png"))); //$NON-NLS-1$
						toolBarBusqueda.add(buttonDeleteMapa);
					}
				}
				{
					scrollPaneMapa = new JScrollPane();
					panelBusqueda.add(scrollPaneMapa, BorderLayout.CENTER);
					miAreaDibujo = new MiAreaDibujo();
					miAreaDibujo.addMouseMotionListener(new MouseMotionAdapter() {
						@Override
						public void mouseDragged(MouseEvent e) {
							
							if (modo == AREA && imagen!=null) {
								((AreaBusquedaGrafico)miAreaDibujo.getUltimoObjetoGrafico()).setX1(e.getX());
								((AreaBusquedaGrafico)miAreaDibujo.getUltimoObjetoGrafico()).setY1(e.getY());
								miAreaDibujo.repaint();
								}
							
						}
					});
					miAreaDibujo.addMouseListener(new MouseAdapter() {
						@Override
						public void mousePressed(MouseEvent e) {
							x = e.getX();
							y = e.getY();
							toolkit = Toolkit.getDefaultToolkit();
							if (imagen != null)
							{
							switch (modo)
							{
							case START:
							miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagStart));
							miAreaDibujo.repaint();
							break;
							case ENCUENTRO:
								miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagEncuentro));
								miAreaDibujo.repaint();
								break;
								
							case PERRO:
								miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagPerro));
								miAreaDibujo.repaint();
								break;
							case GRUPO:
								miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagGrupo));
								miAreaDibujo.repaint();
								break;
							
							case COMISARIA:
								miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagPolicia));
								miAreaDibujo.repaint();
								break;
							case TEXTO:
								txtTexto.setBounds(x, y, 200,30);
								txtTexto.setVisible(true);
								txtTexto.requestFocus();
								txtTexto.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent arg) {
								if(!txtTexto.getText().equals("")) //$NON-NLS-1$
								miAreaDibujo.addObjetoGrafico(new TextoGrafico(x, y+15, txtTexto.getText(),
								Color.BLUE));
								txtTexto.setText(""); //$NON-NLS-1$
								txtTexto.setVisible(false);
								miAreaDibujo.repaint();
								}
								});
								miAreaDibujo.add(txtTexto);
								break;
							case AREA:
								miAreaDibujo.addObjetoGrafico(new AreaBusquedaGrafico(x,y,x,y,
										Color.BLUE));
								break;
							
							}
							}
						}
						
						
					});
					miAreaDibujo.setIcon(null);
					scrollPaneMapa.setViewportView(miAreaDibujo);
					
					
				}
			}
			{
				panelAyuda = new JPanel();
				tabbedPane.addTab(Messages.getString("MenuPrincipal.175"), new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/Ayuda.gif")), panelAyuda, null); //$NON-NLS-1$ //$NON-NLS-2$
				panelAyuda.setLayout(new BorderLayout(0, 0));
				{
					txtrAplicacinCreadaPor = new JTextArea();
					txtrAplicacinCreadaPor.setText(Messages.getString("MenuPrincipal.2")); //$NON-NLS-1$
					panelAyuda.add(txtrAplicacinCreadaPor, BorderLayout.CENTER);
				}
			}
			{
				panelPerdidos = new JPanel();
				tabbedPane.addTab(Messages.getString("MenuPrincipal.180"), new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/PerdidoPerro.jpg")), panelPerdidos, null); //$NON-NLS-1$ //$NON-NLS-2$
				panelPerdidos.setLayout(new BorderLayout(0, 0));
				{
					toolBar_Perdidos = new JToolBar();
					panelPerdidos.add(toolBar_Perdidos, BorderLayout.SOUTH);
					{
						btnAadirPerdido = new JButton(Messages.getString("MenuPrincipal.182")); //$NON-NLS-1$
						btnAadirPerdido.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								
									MiModelo modeloTabla = (MiModelo) tablePerdidos.getModel();
									Object[] nuevaFila= {"Sombra2", "labrador", new //$NON-NLS-1$ //$NON-NLS-2$
											ImageIcon(getClass().getClassLoader().getResource
											("presentacion/labrador.jpg")),Messages.getString("MenuPrincipal.186"),Messages.getString("MenuPrincipal.187"),"torreon","enero 2015","juan","675849384"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
									modeloTabla.aniadeFila(nuevaFila);
									modeloTabla.fireTableDataChanged();
							}
						});
						btnAadirPerdido.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/ic_menu_btn_add.png"))); //$NON-NLS-1$
						toolBar_Perdidos.add(btnAadirPerdido);
					}
					{
						btnBorrarPerdidos = new JButton(Messages.getString("MenuPrincipal.193")); //$NON-NLS-1$
						btnBorrarPerdidos.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								int valor= JOptionPane.showConfirmDialog(framer	, Messages.getString("MenuPrincipal.194")); //$NON-NLS-1$
								if( valor == 0){
								MiModelo modeloTabla = (MiModelo)tablePerdidos.getModel();
								int n= tablePerdidos.getSelectedRow();
								if (n != -1) modeloTabla.eliminaFila(tablePerdidos.getSelectedRow());
								modeloTabla.fireTableDataChanged();
								}
							}
						});
						btnBorrarPerdidos.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/ic_menu_delete.png"))); //$NON-NLS-1$
						toolBar_Perdidos.add(btnBorrarPerdidos);
					}
					{
						btnModificarPerdido = new JButton(Messages.getString("MenuPrincipal.196")); //$NON-NLS-1$
						btnModificarPerdido.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/Modificar.gif"))); //$NON-NLS-1$
						toolBar_Perdidos.add(btnModificarPerdido);
					}
					{
						btnSalirPerdido = new JButton(Messages.getString("MenuPrincipal.198")); //$NON-NLS-1$
						btnSalirPerdido.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								int valor= JOptionPane.showConfirmDialog(framer	, Messages.getString("MenuPrincipal.199")); //$NON-NLS-1$
								if( valor == 0){
									System.exit(0);
								}
							}
						});
						btnSalirPerdido.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/exit.png"))); //$NON-NLS-1$
						toolBar_Perdidos.add(btnSalirPerdido);
					}
				}
				{
					scrollPane_1Perdidos = new JScrollPane();
					panelPerdidos.add(scrollPane_1Perdidos, BorderLayout.CENTER);
					{
						tablePerdidos = new JTable();
						tablePerdidos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
						MiModelo modeloTabla = new MiModelo();
						tablePerdidos.setModel(modeloTabla);
						
						scrollPane_1Perdidos.setViewportView(tablePerdidos);
						
						tablePerdidos.setRowHeight(90);
						{
							panelDonativos = new JPanel();
							tabbedPane.addTab(Messages.getString("MenuPrincipal.201"), new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/Donativo.png")), panelDonativos, null); //$NON-NLS-1$ //$NON-NLS-2$
							panelDonativos.setLayout(new BorderLayout(0, 0));
							{
								panel_Dona = new JPanel();
								panelDonativos.add(panel_Dona, BorderLayout.CENTER);
								panel_Dona.setLayout(null);
								{
									lblContruccinDeCasetas = new JLabel(Messages.getString("MenuPrincipal.203")); //$NON-NLS-1$
									lblContruccinDeCasetas.setBounds(53, 87, 145, 29);
									panel_Dona.add(lblContruccinDeCasetas);
								}
								{
									spinner_1Casetas = new JSpinner();
									spinner_1Casetas.setBounds(208, 87, 49, 27);
									panel_Dona.add(spinner_1Casetas);
								}
								{
									lblS = new JLabel("€ "); //$NON-NLS-1$
									lblS.setBounds(267, 94, 46, 14);
									panel_Dona.add(lblS);
								}
								{
									lblReparacinDePiscina = new JLabel(Messages.getString("MenuPrincipal.205")); //$NON-NLS-1$
									lblReparacinDePiscina.setBounds(53, 141, 134, 26);
									panel_Dona.add(lblReparacinDePiscina);
								}
								{
									spinner_1 = new JSpinner();
									spinner_1.setBounds(208, 138, 49, 27);
									panel_Dona.add(spinner_1);
								}
								{
									label = new JLabel("€ "); //$NON-NLS-1$
									label.setBounds(267, 141, 46, 14);
									panel_Dona.add(label);
								}
								{
									lblCreacinDePeluquera = new JLabel(Messages.getString("MenuPrincipal.207")); //$NON-NLS-1$
									lblCreacinDePeluquera.setBounds(53, 199, 134, 14);
									panel_Dona.add(lblCreacinDePeluquera);
								}
								{
									spinner_2 = new JSpinner();
									spinner_2.setBounds(208, 193, 49, 27);
									panel_Dona.add(spinner_2);
								}
								{
									label_1 = new JLabel("€ "); //$NON-NLS-1$
									label_1.setBounds(267, 199, 46, 14);
									panel_Dona.add(label_1);
								}
								{
									lblNuevasInstalaciones = new JLabel(Messages.getString("MenuPrincipal.209")); //$NON-NLS-1$
									lblNuevasInstalaciones.setBounds(53, 257, 122, 14);
									panel_Dona.add(lblNuevasInstalaciones);
								}
								{
									spinner_3 = new JSpinner();
									spinner_3.setBounds(208, 251, 49, 27);
									panel_Dona.add(spinner_3);
								}
								{
									label_2 = new JLabel("€ "); //$NON-NLS-1$
									label_2.setBounds(267, 257, 46, 14);
									panel_Dona.add(label_2);
								}
								{
									lblNuevaClinicaVeterinaria = new JLabel(Messages.getString("MenuPrincipal.211")); //$NON-NLS-1$
									lblNuevaClinicaVeterinaria.setBounds(53, 316, 145, 14);
									panel_Dona.add(lblNuevaClinicaVeterinaria);
								}
								{
									spinner_4 = new JSpinner();
									spinner_4.setBounds(208, 310, 49, 27);
									panel_Dona.add(spinner_4);
								}
								{
									label_3 = new JLabel("€ "); //$NON-NLS-1$
									label_3.setBounds(267, 313, 46, 14);
									panel_Dona.add(label_3);
								}
								{
									lblFormasDePago = new JLabel(Messages.getString("MenuPrincipal.213")); //$NON-NLS-1$
									lblFormasDePago.setForeground(Color.BLACK);
									lblFormasDePago.setBackground(Color.WHITE);
									lblFormasDePago.setBounds(383, 69, 170, 29);
									panel_Dona.add(lblFormasDePago);
								}
								{
									btnPaypal = new JButton(""); //$NON-NLS-1$
									btnPaypal.setBackground(Color.BLUE);
									btnPaypal.setForeground(Color.GREEN);
									btnPaypal.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/Paypal.png"))); //$NON-NLS-1$
									btnPaypal.setBounds(381, 121, 122, 79);
									panel_Dona.add(btnPaypal);
								}
								{
									buttonTarjeta = new JButton(""); //$NON-NLS-1$
									buttonTarjeta.setBackground(Color.ORANGE);
									buttonTarjeta.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/Visa.png"))); //$NON-NLS-1$
									buttonTarjeta.setBounds(383, 234, 118, 80);
									panel_Dona.add(buttonTarjeta);
								}
								{
									btnPagar = new JButton(Messages.getString("MenuPrincipal.218")); //$NON-NLS-1$
									btnPagar.addActionListener(new ActionListener() {
										public void actionPerformed(ActionEvent e) {
											JOptionPane.showMessageDialog(framer, Messages.getString("MenuPrincipal.219"),Messages.getString("MenuPrincipal.220"),JOptionPane.WARNING_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
										}
									});
									btnPagar.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/Donativo.png"))); //$NON-NLS-1$
									btnPagar.setBounds(172, 411, 268, 41);
									panel_Dona.add(btnPagar);
								}
								{
									lblTotal = new JLabel("Total"); //$NON-NLS-1$
									lblTotal.setBounds(53, 360, 62, 14);
									panel_Dona.add(lblTotal);
								}
								{
									label_4 = new JLabel("€ "); //$NON-NLS-1$
									label_4.setBounds(267, 360, 46, 14);
									panel_Dona.add(label_4);
								}
								{
									spinner_Total = new JSpinner();
									spinner_Total.setBounds(208, 357, 49, 27);
									panel_Dona.add(spinner_Total);
								}
							}
						}
						{
							panel_Padrinos = new JPanel();
							tabbedPane.addTab(Messages.getString("MenuPrincipal.224"), new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/Padrino.jpg")), panel_Padrinos, null); //$NON-NLS-1$ //$NON-NLS-2$
							panel_Padrinos.setLayout(new BorderLayout(0, 0));
							{
								toolBar_1Padrino = new JToolBar();
								panel_Padrinos.add(toolBar_1Padrino, BorderLayout.SOUTH);
								{
									btnAadirPadrino = new JButton(Messages.getString("MenuPrincipal.226")); //$NON-NLS-1$
									btnAadirPadrino.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/ic_menu_btn_add.png"))); //$NON-NLS-1$
									toolBar_1Padrino.add(btnAadirPadrino);
								}
								{
									btnBorrarPadrino = new JButton(Messages.getString("MenuPrincipal.228")); //$NON-NLS-1$
									btnBorrarPadrino.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/ic_menu_delete.png"))); //$NON-NLS-1$
									toolBar_1Padrino.add(btnBorrarPadrino);
								}
								{
									btnModificarPadrino = new JButton(Messages.getString("MenuPrincipal.230")); //$NON-NLS-1$
									btnModificarPadrino.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/Modificar.gif"))); //$NON-NLS-1$
									toolBar_1Padrino.add(btnModificarPadrino);
								}
								{
									btnSalir_1 = new JButton(Messages.getString("MenuPrincipal.232")); //$NON-NLS-1$
									btnSalir_1.addActionListener(new ActionListener() {
										public void actionPerformed(ActionEvent e) {
											int valor= JOptionPane.showConfirmDialog(framer	, Messages.getString("MenuPrincipal.233")); //$NON-NLS-1$
											if( valor == 0){
												System.exit(0);
											}
										}
									});
									btnSalir_1.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/exit.png"))); //$NON-NLS-1$
									toolBar_1Padrino.add(btnSalir_1);
								}
							}
							{
								panel_PadrinInfo = new JPanel();
								panel_Padrinos.add(panel_PadrinInfo, BorderLayout.CENTER);
								panel_PadrinInfo.setLayout(null);
								{
									lblNombreDelPadrino = new JLabel(Messages.getString("MenuPrincipal.235")); //$NON-NLS-1$
									lblNombreDelPadrino.setBounds(24, 63, 113, 14);
									panel_PadrinInfo.add(lblNombreDelPadrino);
								}
								{
									textField_1 = new JTextField();
									textField_1.setBounds(159, 60, 113, 20);
									panel_PadrinInfo.add(textField_1);
									textField_1.setColumns(10);
								}
								{
									lblDomicilio_1 = new JLabel(Messages.getString("MenuPrincipal.236")); //$NON-NLS-1$
									lblDomicilio_1.setBounds(24, 105, 68, 14);
									panel_PadrinInfo.add(lblDomicilio_1);
								}
								{
									textField_2 = new JTextField();
									textField_2.setBounds(86, 102, 113, 20);
									panel_PadrinInfo.add(textField_2);
									textField_2.setColumns(10);
								}
								{
									lblNumero = new JLabel("N"); //$NON-NLS-1$
									lblNumero.setBounds(214, 105, 46, 14);
									panel_PadrinInfo.add(lblNumero);
								}
								{
									spinner_5 = new JSpinner();
									spinner_5.setBounds(234, 102, 38, 20);
									panel_PadrinInfo.add(spinner_5);
								}
								{
									lblTelefono_1 = new JLabel(Messages.getString("MenuPrincipal.238")); //$NON-NLS-1$
									lblTelefono_1.setBounds(24, 149, 68, 14);
									panel_PadrinInfo.add(lblTelefono_1);
								}
								{
									textField_3 = new JTextField();
									textField_3.setBounds(86, 146, 113, 20);
									panel_PadrinInfo.add(textField_3);
									textField_3.setColumns(10);
								}
								{
									lblNumeroDeCuenta = new JLabel(Messages.getString("MenuPrincipal.239")); //$NON-NLS-1$
									lblNumeroDeCuenta.setBounds(24, 195, 52, 14);
									panel_PadrinInfo.add(lblNumeroDeCuenta);
								}
								{
									textField_4 = new JTextField();
									textField_4.setBounds(86, 192, 113, 20);
									panel_PadrinInfo.add(textField_4);
									textField_4.setColumns(10);
								}
								{
									lblPint = new JLabel(Messages.getString("MenuPrincipal.240")); //$NON-NLS-1$
									lblPint.setBounds(24, 233, 46, 14);
									panel_PadrinInfo.add(lblPint);
								}
								{
									textField_5 = new JTextField();
									textField_5.setBounds(51, 230, 86, 20);
									panel_PadrinInfo.add(textField_5);
									textField_5.setColumns(10);
								}
								{
									JLabel label_FotoPadrino = new JLabel(""); //$NON-NLS-1$
									label_FotoPadrino.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/Voluntario2.jpg"))); //$NON-NLS-1$
									label_FotoPadrino.setBounds(332, 63, 263, 231);
									panel_PadrinInfo.add(label_FotoPadrino);
								}
								{
									lblFormaDePago = new JLabel(Messages.getString("MenuPrincipal.243")); //$NON-NLS-1$
									lblFormaDePago.setBounds(24, 280, 125, 14);
									panel_PadrinInfo.add(lblFormaDePago);
								}
								{
									comboBox_1 = new JComboBox();
									comboBox_1.setModel(new DefaultComboBoxModel(new String[] {Messages.getString("MenuPrincipal.244"), Messages.getString("MenuPrincipal.245")})); //$NON-NLS-1$ //$NON-NLS-2$
									comboBox_1.setBounds(160, 277, 89, 20);
									panel_PadrinInfo.add(comboBox_1);
								}
								{
									lblAportacinMensual = new JLabel(Messages.getString("MenuPrincipal.246")); //$NON-NLS-1$
									lblAportacinMensual.setBounds(24, 336, 125, 14);
									panel_PadrinInfo.add(lblAportacinMensual);
								}
								{
									spinner_6 = new JSpinner();
									spinner_6.setBounds(159, 333, 50, 20);
									panel_PadrinInfo.add(spinner_6);
								}
								{
									lblNombreDePerro = new JLabel(Messages.getString("MenuPrincipal.247")); //$NON-NLS-1$
									lblNombreDePerro.setBounds(24, 384, 148, 14);
									panel_PadrinInfo.add(lblNombreDePerro);
								}
								{
									textField_6 = new JTextField();
									textField_6.setBounds(131, 381, 89, 20);
									panel_PadrinInfo.add(textField_6);
									textField_6.setColumns(10);
								}
								{
									btnFoto = new JButton(Messages.getString("MenuPrincipal.248")); //$NON-NLS-1$
									btnFoto.addActionListener(new ActionListener() {
										public void actionPerformed(ActionEvent arg0) {
											JFileChooser fcAbrir2 = new JFileChooser();
											fcAbrir2.setFileFilter(new ImagenFiltro());

											int valorDevuelto = fcAbrir2.showOpenDialog(framer);
											if (valorDevuelto == JFileChooser.APPROVE_OPTION) {
											File file2 = fcAbrir2.getSelectedFile();
											labelFotoVolunt.setIcon(new ImageIcon(file2.getAbsolutePath()));
											}
										}
									});
									btnFoto.setIcon(new ImageIcon(MenuPrincipal.class.getResource("/Presentacion/cargarFoto.png"))); //$NON-NLS-1$
									btnFoto.setBounds(332, 325, 263, 37);
									panel_PadrinInfo.add(btnFoto);
								}
							}
						}
						
						Object[] fila1= {"Sombra", "Border Collie", new //$NON-NLS-1$ //$NON-NLS-2$
								ImageIcon(getClass().getClassLoader().getResource
								("presentacion/borderCollie.jpg")),Messages.getString("MenuPrincipal.253"),Messages.getString("MenuPrincipal.254"),"torreon","enero 2015","juan","675849384"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
								modeloTabla.aniadeFila(fila1);
								
								TableColumn columnaFoto = tablePerdidos.getColumnModel().getColumn(2);
								columnaFoto.setCellEditor(new ColumnaFotoEditor());
								
								
								TableColumn columnaRazas = tablePerdidos.getColumnModel().getColumn(1);
								JComboBox comboBox = new JComboBox();
								comboBox.addItem("Border Collie"); //$NON-NLS-1$
								comboBox.addItem("Galgo"); //$NON-NLS-1$
								comboBox.addItem("Labrador"); //$NON-NLS-1$
								comboBox.addItem("Mestizo"); //$NON-NLS-1$
								comboBox.addItem("Pastor Alemán"); //$NON-NLS-1$
								columnaRazas.setCellEditor(new DefaultCellEditor(comboBox));
								
								TableColumn columnaSexo = tablePerdidos.getColumnModel().getColumn(3);
								JComboBox comboBox2= new JComboBox();
								comboBox2.addItem(Messages.getString("MenuPrincipal.264")); //$NON-NLS-1$
								comboBox2.addItem(Messages.getString("MenuPrincipal.265")); //$NON-NLS-1$
							
								columnaSexo.setCellEditor(new DefaultCellEditor(comboBox2));
								
								TableColumn columnaTamaño = tablePerdidos.getColumnModel().getColumn(4);
								JComboBox comboBox3= new JComboBox();
								comboBox3.addItem(Messages.getString("MenuPrincipal.266")); //$NON-NLS-1$
								comboBox3.addItem(Messages.getString("MenuPrincipal.267")); //$NON-NLS-1$
								comboBox3.addItem(Messages.getString("MenuPrincipal.268")); //$NON-NLS-1$

							
								columnaTamaño.setCellEditor(new DefaultCellEditor(comboBox3));
					}
				}
			}
		}
		class FrameWindowListener extends WindowAdapter {
			@Override
			public void windowClosing(WindowEvent arg0) {
			JOptionPane.showMessageDialog(contentPane, Messages.getString("MenuPrincipal.269"), Messages.getString("MenuPrincipal.270"), //$NON-NLS-1$ //$NON-NLS-2$
			JOptionPane.PLAIN_MESSAGE);
			}
			}
	}
}
