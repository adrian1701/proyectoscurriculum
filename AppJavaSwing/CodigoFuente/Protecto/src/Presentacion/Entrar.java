package Presentacion;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.TitledBorder;

public class Entrar {

	private JFrame frame;
	private JPanel panel;
	private JLabel labelFotoInicio;
	private JLabel lblUsuario;
	private JLabel lblContrasea;
	private JTextField textFieldUsuario;
	private JTextField textContrasena;
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	private JCheckBox chckbxRecordarContrasea;
	private JLabel labelEstado;
	private JButton btnEntrar;
	private final String password = "contra123"; //$NON-NLS-1$

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Entrar window = new Entrar();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Entrar() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 555, 351);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		{
			panel = new JPanel();
			panel.setBorder(new TitledBorder(null, Messages.getString("Entrar.1"), TitledBorder.LEADING, TitledBorder.TOP, null, null)); //$NON-NLS-1$
			panel.setBounds(10, 11, 519, 301);
			frame.getContentPane().add(panel);
			panel.setLayout(null);
			{
				labelFotoInicio = new JLabel(""); //$NON-NLS-1$
				labelFotoInicio.setIcon(new ImageIcon(Entrar.class.getResource("/Presentacion/PerroInicio.jpg"))); //$NON-NLS-1$
				labelFotoInicio.setBounds(25, 66, 117, 123);
				panel.add(labelFotoInicio);
			}
			{
				lblUsuario = new JLabel(Messages.getString("Entrar.4")); //$NON-NLS-1$
				lblUsuario.setBounds(153, 79, 76, 14);
				panel.add(lblUsuario);
			}
			{
				lblContrasea = new JLabel(Messages.getString("Entrar.5")); //$NON-NLS-1$
				lblContrasea.setEnabled(false);
				lblContrasea.setBounds(152, 116, 77, 14);
				panel.add(lblContrasea);
			}
			{
				textFieldUsuario = new JTextField();
				textFieldUsuario.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
								//Activamos los de la contraseña
								lblContrasea.setEnabled(true);
								textContrasena.setEnabled(true);
								//Pasamos el foco (el cursor) al campo de la contraseña
								textContrasena.requestFocus();
					}
				});
				textFieldUsuario.setBounds(239, 76, 117, 20);
				panel.add(textFieldUsuario);
				textFieldUsuario.setColumns(10);
			}
			{
				textContrasena = new JTextField();
				textContrasena.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
								if
								(String.valueOf(textContrasena.getText()).toString().equals(password))
								{
								labelEstado.setBackground(Color.GREEN);
								labelEstado.setText(Messages.getString("Entrar.6")); //$NON-NLS-1$
								labelEstado.setVisible(true);
								btnEntrar.setEnabled(true);
								lblContrasea.setEnabled(false);
								textContrasena.setEnabled(false);
								} else {
									labelEstado.setBackground(Color.YELLOW);
									labelEstado.setText(Messages.getString("Entrar.7")); //$NON-NLS-1$
									labelEstado.setVisible(true);
								btnEntrar.setEnabled(false);
								}
					}
				});
				textContrasena.setEnabled(false);
				textContrasena.setBounds(239, 113, 117, 20);
				panel.add(textContrasena);
				textContrasena.setColumns(10);
			}
			{
				chckbxRecordarContrasea = new JCheckBox(Messages.getString("Entrar.8")); //$NON-NLS-1$
				chckbxRecordarContrasea.setSelected(true);
				chckbxRecordarContrasea.setBounds(239, 152, 149, 23);
				panel.add(chckbxRecordarContrasea);
			}
			{
				labelEstado = new JLabel("");
				labelEstado.setOpaque(true);
				labelEstado.setBounds(179, 201, 259, 39);
				panel.add(labelEstado);
			}
			{
				btnEntrar = new JButton(Messages.getString("Entrar.10")); //$NON-NLS-1$
				btnEntrar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						//Se crea una instancia de la segunda ventana (JFrame)
						MenuPrincipal menu = new MenuPrincipal();
						//se hace visible
						menu.setVisible(true);
						
						
						//se destruye la ventana actual (atributo a nivel de clase)
						frame.dispose();
					}
				});
				btnEntrar.setEnabled(false);
				btnEntrar.setBounds(382, 79, 89, 51);
				panel.add(btnEntrar);
			}
		}
	}
}
