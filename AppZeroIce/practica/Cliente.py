#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('drobots.ice')
import drobots
import struct
import random
Ice.loadSlice('-I. --all RobotFactory.ice')
import Services



class PlayerI(drobots.Player):
	

	def __init__(self,proxyContainerProxies):
		self.proxyContainerProxies = proxyContainerProxies
		
	def makeController(self,robot, current=None):
		
	   	
		# se busca dentro del contenedorPrincipal el proxy de la factoria a traves de la key

		com = current.adapter.getCommunicator()
		proxyCP = com.stringToProxy(self.proxyContainerProxies)
        	container = Services.ContainerPrx.checkedCast(proxyCP)

		keys = iter(container.list().keys())
		valores = iter(container.list().values())

		while True:
   			try:
				key = keys.next()
				valor = valores.next()

    			except StopIteration:
        			break
				
			if key == "proxieFactoria":
				proxyF = valor
		
		
		
		#una vez obtenido se construye un objeto factoria y se delega la ejeccución al programa factoria
		
		factory = drobots.RobotFactoryPrx.checkedCast(proxyF)
		robotController = factory.make(robot)
		
	
		return robotController
	
   
	def win(self,current=None):
		print("Has ganado.")

	def lose(self,current=None):
		print("Has perdido")
	def gameAbort(self,current = None):
		print(" Juego abortado")


 
	
		
	



class Cliente(Ice.Application):
    def run(self, argv):
		
	broker = self.communicator()
        proxy =broker.stringToProxy(argv[1])
	proxyContainerProxies = argv[2]

	# es el programa principal
	
	# entra en la partida a traves del proxy que se pasa como argumento
        game = drobots.GamePrx.checkedCast(proxy)
	

        if not game:
            raise RuntimeError('Invalid proxy')
	
	
	
	# crea un jugador para unirse  la partida
	
	#EL segundo argumento representa al proxy del contenedorPrincipal
	# el cual a su vez contiene el proxyFactoria, lo cual
	# permite a este programa delegar sus tareas a la factoria
	# el proxy del contenedor se pasa como argumento del contructor en el sirviente de player

	adapter=broker.createObjectAdapterWithEndpoints("PlayerAdapter","default")
	servant = PlayerI(proxyContainerProxies)
	adapter.activate()
	
	
	proxyPlayer = adapter.addWithUUID(servant)
	player = drobots.PlayerPrx.checkedCast(proxyPlayer)
	
	

	
	
	
	valor = "peokd4"
	game.login(player,valor)
	

	


	self.shutdownOnInterrupt()
	broker.waitForShutdown()

        return 0
	

sys.exit(Cliente().main(sys.argv))















