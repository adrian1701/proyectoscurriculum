
package practicaSistemas;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Stack;

public class Solucion {


	public static void solucionar(Nodo nodoFin, long nNodos, double tiempo){
		
		Stack<String> camino = new Stack<String>();
		int prof = nodoFin.getProfundidad();
		float costo = nodoFin.getCosto();

		//Si el nodo inicial == nodoFinla -->costo=0
		
		
		while(nodoFin.getPadre() != null){
			camino.push(nodoFin.getAccion() + " Altitud: " + nodoFin.getEstado().getAltura());
			nodoFin = nodoFin.getPadre();
		}
		
		
		
		FileWriter fichero = null;
	    PrintWriter pw = null;
	    try{
	    	fichero = new FileWriter("solucion.txt");
	    	pw = new PrintWriter(fichero);
	    	if (costo == 0.0) pw.print("El  origen es el mismo que el  destino");
	    	else{
	    		pw.println("Nodos: " +nNodos+ " Costo: " + costo + " m Profundidad: " + prof + " Tiempo tardado: " + tiempo + " ns");
	    		pw.println("Camino: ");
	    		while(camino.size() != 0){
	    			pw.println(camino.pop() + " ");
	    		}
	    	}
	    	pw.close();
	    }catch(Exception e){
	    	e.printStackTrace();
	    }
	    System.out.println("Soluci�n encontrada y escrita en 'solucion.txt'");
	}
}
