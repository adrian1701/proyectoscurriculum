package practicaSistemas;


import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.PriorityQueue;
import java.util.Scanner;

import ncsa.hdf.object.h5.*;
import ncsa.hdf.object.FileFormat;
import ncsa.hdf.object.Dataset;
import ncsa.hdf.hdf5lib.H5;
import ncsa.hdf.hdf5lib.HDF5Constants;


public class Util {
	private static String FILENAME = "H5_UTM.h5";
	private static String GROUPNAME = "UMT";
	private static ArrayList<String> group_names = new ArrayList<String>();
	private static ArrayList<String> dset_names = new ArrayList<String>();
	private static int file_id=-1;
	private static Hashtable<String, Nodo> historico= new Hashtable<String, Nodo>();
	
	
	
	
	
	
	public static String[] datos(){
		Scanner teclado = new Scanner(System.in);
		String[] dats = new String[7];
		
		System.out.println("Introduzca la coordenada UTM X de inicio: ");
		dats[0] = teclado.next();
		
		System.out.println("Introduzca la coordenada UTM Y de inicio: ");
		dats[1] = teclado.next();
		
		System.out.println("Introduzca la coordenada UTM X de destino: ");
		dats[2] = teclado.next();
		
		System.out.println("Introduzca la coordenada UTM Y de destino: ");
		dats[3] = teclado.next();
		
		System.out.println("Introduzca la estrategia: ");
		System.out.println("Las posibles estrategias son: 'profundidad' 'anchura 'uniforme' 'voraz' 'A'");
		dats[4] = teclado.next();
		
		System.out.println("Si desea que se realice con optimizaci�n, teclee 'opt'");
		dats[5] = teclado.next();
		
		System.out.println("Introduce la profundidad maxima.");
		dats[6] = teclado.next();
		
		teclado.close();
		
		
		return dats;
	}
	

	
	
	
	
	public static Hashtable<String, Nodo> getHistorico() {
		return historico;
	}
	
	
	
	
	
	public static void inicializacion(){
		int genGroup_id = -1;
		int group_id = -1;
		int dataset_id= -1;
		int attribute_id= -1;
		int dataspace_id = -1;
		long[] dims = { 5 };
		int[] attr_data = new int[5];
		long[] dimenDataset = new long[2];
		Scanner teclado = new Scanner(System.in);
		System.out.println("Introduzca la ruta donde se encuentren los ficheros .asc");
		System.out.println("Ejemplo: C:/Users/adri/downloads/ficheros ");
		System.out.println("Nota: la carpeta ficheros debe contener unicamente los ficheros necesarios para ejecutar.");
		
		String dir = teclado.next();
		
		File f = new File(dir);
		File[] ficheros = f.listFiles();
		
		
		// Creamos nuevo fichero .h5
		try {
			file_id = H5.H5Fcreate(FILENAME, HDF5Constants.H5F_ACC_TRUNC,
							HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		//Creamos el grupo superior o raiz
		try {
			if (file_id >= 0)
				genGroup_id = H5.H5Gcreate(file_id, "/" + GROUPNAME, HDF5Constants.H5P_DEFAULT, 
						HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);
				group_names.add("/" + GROUPNAME);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		
		for(int i=0; i<ficheros.length; i++){
		//Leemos los atributos del grupo 
			float[][] dats=null;
			String pagina = null;
			try{
				Scanner sc = new Scanner(new FileReader(ficheros[i]));
				// filas
				sc.next();
				attr_data[0]=sc.nextInt();
				sc.next();
				// columnas
				attr_data[1]=sc.nextInt();
				sc.next();
				attr_data[2]=sc.nextInt();
				sc.next();//centro utm y
				attr_data[3]=sc.nextInt();
				sc.next();//tama�o celda
				attr_data[4]=sc.nextInt();
				sc.nextLine();
				sc.nextLine();
				pagina = ficheros[i].getName().substring(6, 10);
				//contenido de la matriz
				dimenDataset[0] = attr_data[1];
				dimenDataset[1] = attr_data[0];
				dats = new float[attr_data[1]][attr_data[0]];
				while(sc.hasNext()){
					for(int i2=0; i2<attr_data[1]; i2++)
						for(int j=0; j<attr_data[0]; j++){
							String sig = sc.next();
							if (Character.isDigit(sig.charAt(0)))
							dats[i2][j]=Float.parseFloat(sig);	
						}
				}
				sc.close();
			}catch(java.util.NoSuchElementException e){
				
			}catch(Exception e){
				e.printStackTrace();
			}
		
			//Creacion de grupo
			try {
				if (file_id >= 0)
					group_id=H5.H5Gcreate(file_id, "/" + GROUPNAME + "/" + pagina,
							HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);
					group_names.add("/" + GROUPNAME + "/" + pagina);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
				
		//dataspace para el atributo del grupo
			try {
				dataspace_id = H5.H5Screate_simple(1, dims, null);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		
		//Creacion del atributo en el grupo
			try {
				if ((group_id >= 0) && (dataspace_id >= 0))
					attribute_id=H5.H5Acreate(group_id, "Cabecera",
						HDF5Constants.H5T_STD_I32BE, dataspace_id,
						HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);
				}
			catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				if (attribute_id >= 0)
				H5.H5Awrite(attribute_id, HDF5Constants.H5T_NATIVE_INT, attr_data);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			
		// Create the data space for the first dataset.
			try {
				dataspace_id = H5.H5Screate_simple(2, dimenDataset, null);
			}
			catch (Exception e) {
				e.printStackTrace();
			}

			//Create the dataset
			try {
				if ((file_id >= 0) && (dataspace_id >= 0))
					dataset_id=H5.H5Dcreate(file_id,
							"/" + GROUPNAME + "/" + pagina + "/dset" + pagina, HDF5Constants.H5T_IEEE_F32BE,
							dataspace_id, HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);
				dset_names.add("/" + GROUPNAME + "/" + pagina + "/dset" + i);
			}
			catch (Exception e) {
				e.printStackTrace();
			}

			// Write the first dataset.
			try {
				if (dataset_id >= 0)
					H5.H5Dwrite(dataset_id, HDF5Constants.H5T_NATIVE_FLOAT,
							HDF5Constants.H5S_ALL, HDF5Constants.H5S_ALL,
							HDF5Constants.H5P_DEFAULT, dats);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		
	    //Cierre de todo de manera ordenada
		try{
			if (dataspace_id >= 0)
				H5.H5Sclose(dataspace_id);
			if (attribute_id >= 0)
				H5.H5Aclose(attribute_id);
			if (dataset_id >= 0)
				H5.H5Dclose(dataset_id);
			if(group_id >= 0)
				H5.H5Gclose(group_id);
				
		}catch (Exception e) {
			e.printStackTrace();
		}
	
	}
		
		try{
			if(genGroup_id >= 0)
				H5.H5Gclose(genGroup_id);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		try {
			if (file_id >= 0)
				H5.H5Fclose(file_id);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	public static ArrayList<String> getGroup_names() {
		return group_names;
	}
	
	
	
	public static void setGroup_names(ArrayList<String> group_names) {
		Util.group_names = group_names;
	}
	
	
	
	
	
	
	public static LugarGeografico lugarGeografico(int umtX, int umtY){
		int attr[];
		int x=0, y=0;
		String pagina=null;
		
		for(int i=1; i<group_names.size(); i++){
			
			//obtenemos los datos del group names, que previamente hemos introducido.
			attr=atributos(group_names.get(i));
			int xll=attr[2];
			int yll=attr[3];
			
			//Si pertenece a una hora se le asignan los valores a x e y.
			if(umtX <= (((attr[0]-1)*25) + xll) && umtX >= xll && umtY <= (((attr[1]-1)*25) + yll) && umtY >= yll){
				pagina = group_names.get(i).substring(5, 9);
				x=(umtX - xll)/25;
				y=((umtY-yll)/25);
			}
		}
		//Si no pertenece a ninguna pagina, devolvemos null
		if(pagina==null) return null;
		else return new LugarGeografico(pagina, x, y);
	}
	
	
	
	
	
	
	
	public static int[] utm(LugarGeografico lugar){
		int dset_data[];
		int grupo=0;
		int cutm[]= new int[2];
		dset_data = new int[5];
		for (int i=0; i<group_names.size(); i++)
			if(group_names.get(i).equals("/"+GROUPNAME+"/"+lugar.getHoja()))
				grupo = i;
		
	
		dset_data=atributos(group_names.get(grupo));
		
		cutm[0]=dset_data[2] + lugar.getX()*dset_data[4];  // el 2 es xll  y el 4 es cellsize.
		cutm[1]=dset_data[3] + lugar.getY()*dset_data[4];  // el 3 es yll  y el 4 es cellsize.
		
		return cutm;
	}

	
	
	
	
	
	
	
	public static float altura(LugarGeografico lugar){
		float[] data=null;
		H5File file;
		Dataset dset=null;
		int[] attr;
		
		attr=atributos("/"+GROUPNAME+"/"+lugar.getHoja());
		
		
		try {
			file = new H5File(FILENAME, FileFormat.READ);
			file_id = H5.H5Fopen(FILENAME, HDF5Constants.H5F_ACC_RDONLY, HDF5Constants.H5P_DEFAULT);
			dset = (Dataset) file.get("/UMT/"+lugar.getHoja()+"/dset"+lugar.getHoja());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		dset.init();
		int rank = dset.getRank(); // rank = 2, a 2D dataset
		long[] count = dset.getSelectedDims();
		long[] start = dset.getStartDims();
		
		for (int i = 0; i < rank; i++) {
			start[1] =lugar.getX();
			start[0]=(attr[1]-1)-lugar.getY();
			count[i] = 1;
		}
		 
		try {
			data =  (float[]) dset.getData();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			if (file_id >= 0)
				H5.H5Fclose(file_id);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return  data[0];

	}
	
	
	
	
	
	
	
	public static int[] atributos(String grupo){
		int group_id=-1;
		int attribute_id=-1;
		int []dset_data;
		
		try{
			file_id = H5.H5Fopen(FILENAME, HDF5Constants.H5F_ACC_RDONLY, 
					HDF5Constants.H5P_DEFAULT);
		}catch(Exception e){
			e.printStackTrace();
		}
		try {
			if (file_id>= 0)
				group_id = H5.H5Gopen(file_id, grupo, HDF5Constants.H5P_DEFAULT);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if (group_id >= 0)
				attribute_id = H5.H5Aopen_by_name(group_id, ".","Cabecera", 
						HDF5Constants.H5P_DEFAULT, HDF5Constants.H5P_DEFAULT);
			}
		catch (Exception e) {
			e.printStackTrace();
		}
		dset_data = new int[5];

	// Obtencion de los atributos
		try {
			if (attribute_id >= 0)
				H5.H5Aread(attribute_id, HDF5Constants.H5T_NATIVE_INT, dset_data);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		//Cerramos atributos y grupos y fichero
		try{
			if (attribute_id >= 0)
				H5.H5Aclose(attribute_id);
			if(group_id >= 0)
				H5.H5Gclose(group_id);
			if(file_id >=0 )
				H5.H5Fclose(file_id);
				
		}catch (Exception e) {
			e.printStackTrace();
		}
		return dset_data;
	}
	
	
	
	
	
	
	
	public static void busqueda(Problema prob, String estrategia, int max_prof, String opt){
		int id=0;
		boolean solucion;
		
		OrdenarCola orden = new OrdenarCola();
		PriorityQueue<Nodo> frontera = new PriorityQueue<Nodo>(10, orden);
		
		
		double inic = System.nanoTime();
		
		Nodo raiz = new Nodo( new Sucesor( prob.getInicial(),"", 0));
		frontera.offer(raiz);
		solucion=false;
		Nodo actual=null;
		
		while(!solucion && !frontera.isEmpty()){
			actual = frontera.poll();
			if(prob.esMeta(actual.getEstado()))
				solucion=true;
			else{
				if(actual.getProfundidad()<max_prof){
					ArrayList<Sucesor> sucesores = EspacioEstados.sucesores(actual.getEstado());
					ArrayList<Nodo> nodos= crearLista(opt, actual, sucesores, id, estrategia, prob);
					
					id += nodos.size();
					
					for(int i=0; i<nodos.size(); i++){
						frontera.add(nodos.get(i));
					}
				}
			}
		}
		
		double fin = System.nanoTime();
		
		
		if(solucion){
			Solucion.solucionar(actual, id, fin-inic);
		}
		else {
			System.out.println("No existe solucion");
		}
	}
	
	
	
	
	public static ArrayList<Nodo> crearLista(String opt, Nodo actual, ArrayList<Sucesor> sucesores, int id, String estrategia, Problema prob){
		ArrayList<Nodo> nodos = new ArrayList<Nodo>();
		
		if(opt.equals("opt")){
			for(Sucesor suc : sucesores){
				
				Nodo nodo_aux = new Nodo(++id, actual, suc, prob, estrategia);
				//la key es la concatenacion de hoja, x e y
				
				String key = Integer.toString(suc.getEstado().getLugar().getX()) +
						Integer.toString(suc.getEstado().getLugar().getY())+suc.getEstado().getLugar().getHoja();
				
				if(!Util.getHistorico().containsKey(key)){
					Util.getHistorico().put(key, nodo_aux);
					nodos.add(nodo_aux);
				}
				else if(Util.getHistorico().get(key).getValoracion() > nodo_aux.getValoracion())
					Util.getHistorico().get(key).setValoracion(nodo_aux.getValoracion());
			}
		}else{
			for(Sucesor suc : sucesores){
				nodos.add(new Nodo(++id, actual, suc, prob, estrategia));
			}
		}
		return nodos;
		
	}
	
}
