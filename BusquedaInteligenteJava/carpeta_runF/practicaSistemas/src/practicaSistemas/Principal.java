
package practicaSistemas;

import java.util.ArrayList;

public class Principal {
	
	public static void main(String []args){
		
		try{ 
			 
		Util.inicializacion();
		String[] datos = Util.datos();
		LugarGeografico lg = Util.lugarGeografico(Integer.parseInt(datos[0]), Integer.parseInt(datos[1]));
		//En el espacio de estados, funcion sucesores y estado inicial
		EspacioEstados espacio = new EspacioEstados();
 		Problema prob = new Problema(Integer.parseInt(datos[2]), Integer.parseInt(datos[3]), espacio.getEstado(lg));
 		System.out.println("Espere hasta que se encuentre la soluci�n...");
 		Util.busqueda(prob, datos[4], Integer.parseInt(datos[6]), datos[5]);
		}
		 
		 catch(java.lang.NullPointerException e ){
				System.out.println("Se han introducido los datos de manera err�nea, Reinicia el programa.");
				
			}
		 catch(java.lang.NumberFormatException e ){
				System.out.println("Has introducido los tipos de datos de manera err�nea, Reinicia el programa.");
				
			}
		
	}
}
