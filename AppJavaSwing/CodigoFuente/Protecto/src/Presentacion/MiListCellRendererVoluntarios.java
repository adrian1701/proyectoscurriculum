package Presentacion;

import java.awt.Color;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;


	class MiListCellRendererVoluntarios extends DefaultListCellRenderer {
		protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
		public Component getListCellRendererComponent(JList list, Object value, int index,boolean
		isSelected, boolean hasFocus) {
		JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index,
		isSelected, hasFocus);
		
		if (list.getModel().getElementAt(index) == "Pedro") {
			renderer.setIcon(new ImageIcon(MiListCellRendererVoluntarios.class.getResource("Voluntario.jpg")));
			} else
			if (list.getModel().getElementAt(index) == "Cesar"){
			renderer.setIcon(new ImageIcon(MiListCellRendererVoluntarios.class.getResource("Voluntario2.jpg")));
			}
		
		
		renderer.setHorizontalAlignment(JLabel.CENTER);
		renderer.setBackground(new Color(250,250,200));
		renderer.setForeground(Color.BLUE);
		if (isSelected) renderer.setBackground(new Color(200,250,200));
		return renderer;
		}
		}