package practicaSistemas;



public class Nodo {
	
	protected int id;
	protected Nodo padre;
	protected Estado estado;
	protected float valoracion,costo,desnivel;
	protected int profundidad;
	protected String accion;
	protected boolean cambiaDeSentido;
	
	public Nodo (Sucesor sucesor){
		this.estado=sucesor.getEstado();
		this.id=0;
		this.padre=null;
		this.valoracion=0;
		this.cambiaDeSentido=false;
		this.desnivel=0;
		this.profundidad=0;
		this.accion=null;
		this.cambiaDeSentido=false;
	}

	public Nodo(int id, Nodo padre,Sucesor sucesor, Problema problema, String estrategia) {
		super();
		this.id = id;
		this.padre = padre;
		this.estado=sucesor.getEstado();
		this.profundidad=padre.getProfundidad()+1;
		this.accion=sucesor.getAccion();
		this.desnivel=sucesor.getDesnivel();
		this.cambiaDeSentido=sucesor.isCambia();
		
		calcularCosto();
		calcularValoracion(problema, estrategia);
	
	}
	
	public void calcularCosto(){
		
		double ld=0;
		
		if(this.getAccion().length()<2){
			ld=25;
		}
		else{
			ld=Math.sqrt(1250); // (sqrt(2 * 25^2). 
		}
		
		this.costo=(float) Math.sqrt((ld*ld)+(this.desnivel*this.desnivel)); //sqrt(ld^2 + desnivel^2).
		
		if(this.padre.getAccion()!=null && !this.padre.getAccion().equals(this.accion)){
			this.costo=((float)(this.costo*1.1) + this.padre.getCosto());
		}
		else{
			this.costo=this.costo + this.padre.getCosto();
		}
		
	}
	
	
	public void calcularValoracion(Problema problema,String estrategia){
		
		switch(estrategia){
		
		case "anchura":
			this.valoracion=this.profundidad;
			break;
			
		case "uniforme":
			this.valoracion=this.costo;
			break;
			
		case "profundidad":
			this.valoracion=-this.profundidad;
			break;
		
		case "voraz":
			int[] coordenadas = Util.utm(this.estado.getLugar());
			this.valoracion=(float) (Math.sqrt(Math.pow(coordenadas[0]-problema.getUmtX(),2)+Math.pow(coordenadas[1]-problema.getUmtY(), 2)));
			break;
			
		case "A":
			int[]cood=Util.utm(this.estado.getLugar());
			this.valoracion=(float)(Math.sqrt(Math.pow(cood[0]-problema.getUmtX(), 2)+Math.pow(cood[1]-problema.getUmtY(), 2))+this.costo);
			break;
		}
		
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Nodo getPadre() {
		return padre;
	}

	public void setPadre(Nodo padre) {
		this.padre = padre;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public float getValoracion() {
		return valoracion;
	}

	public void setValoracion(float valoracion) {
		this.valoracion = valoracion;
	}

	public float getCosto() {
		return costo;
	}

	public void setCosto(float costo) {
		this.costo = costo;
	}

	public float getDesnivel() {
		return desnivel;
	}

	public void setDesnivel(float desnivel) {
		this.desnivel = desnivel;
	}

	public int getProfundidad() {
		return profundidad;
	}

	public void setProfundidad(int profundidad) {
		this.profundidad = profundidad;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public boolean isCambiaDeSentido() {
		return cambiaDeSentido;
	}

	public void setCambiaDeSentido(boolean cambiaDeSentido) {
		this.cambiaDeSentido = cambiaDeSentido;
	}
	
	
	
}
