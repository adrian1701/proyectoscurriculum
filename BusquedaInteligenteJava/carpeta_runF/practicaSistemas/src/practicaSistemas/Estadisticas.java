package practicaSistemas;


import java.util.ArrayList;
import java.util.PriorityQueue;

public class Estadisticas {
	public static void generarEstadisticas(Problema problema){
		int num=0;
		double min=100, max=0,inicio,fin;
		
		Nodo actual;
		int id=0;
		
		OrdenarCola pqs = new OrdenarCola();
		ArrayList<Nodo>listaNodos=new ArrayList<Nodo>();
		PriorityQueue<Nodo> frontera = new PriorityQueue<Nodo>(10, pqs);
		Nodo raiz = new Nodo(new Sucesor(problema.getInicial(),"",0));
		
		frontera.offer(raiz);
		inicio=System.nanoTime();
		for(;;){
			actual = frontera.poll();
			try{
				
				ArrayList<Sucesor> sucesores = EspacioEstados.sucesores(actual.getEstado());
				listaNodos=Util.crearLista("opt",actual,sucesores,id,"anchura",problema);
				
				
				id += listaNodos.size();
				for(int i=0; i<listaNodos.size(); i++){
					double t1 = System.nanoTime();
					frontera.add(listaNodos.get(i));
					double t2 = System.nanoTime();
					num++;
					if((t2-t1)<min) min = t2-t1;
					else if((t2-t1)>min) max = t2-t1;
				}
			}	
			catch(java.lang.OutOfMemoryError e){
				fin = System.nanoTime();
				System.out.println("Tiempo promedio : " + ((fin-inicio)/num));
				System.out.println("Tiempo maximo: "+ max +"\nTiempo minimo : "+ min);
				System.out.println("Num de nodos: " + num);
				break;
			}
		}
	}

	
}
