package practicaSistemas;

public class Problema {

	protected int umtX,umtY;
	protected Estado inicial;
	protected LugarGeografico lugar;
	
	public Problema(int umtX, int umtY, Estado inicial) {
		super();
		this.umtX = umtX;
		this.umtY = umtY;
		this.inicial = inicial;
		this.lugar = Util.lugarGeografico(this.umtX,this.umtY);
	}

	
	public boolean esMeta(Estado estado){
		boolean result=false;
		
		if(this.lugar.getHoja().equals(estado.getLugar().getHoja()) && this.lugar.getX()== estado.getLugar().getX()
				&& this.lugar.getY()== estado.getLugar().getY()){
			result=true;
		}
		return result;
		
	}
	public int getUmtX() {
		return umtX;
	}

	public void setUmtX(int umtX) {
		this.umtX = umtX;
	}

	public int getUmtY() {
		return umtY;
	}

	public void setUmtY(int umtY) {
		this.umtY = umtY;
	}

	public Estado getInicial() {
		return inicial;
	}

	public void setInicial(Estado inicial) {
		this.inicial = inicial;
	}

	public LugarGeografico getLugar() {
		return lugar;
	}

	public void setLugar(LugarGeografico lugar) {
		this.lugar = lugar;
	}
	
	
	
	
}
