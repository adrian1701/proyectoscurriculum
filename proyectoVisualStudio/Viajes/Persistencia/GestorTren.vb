﻿Imports System.Data.OleDb
Imports System.Collections
Public Class GestorTren
    Private listaTren As Collection
    Private Ag As AgenteBD = AgenteBD.getAgente

    Property Lista
        Get
            Return listaTren
        End Get
        Set(value)

        End Set
    End Property
    Sub New()
        listaTren = New Collection
    End Sub
    Public Sub leerTodo()
        Ag = AgenteBD.getAgente()
        Dim tren As Tren
        Dim lector As OleDbDataReader = Ag.leer("SELECT * FROM trenes ORDER BY Matricula;")
        While lector.Read()
            tren = New Tren(lector(0))
            tren.Tipo.TipoTren = lector(1)
            listaTren.Add(tren)
        End While
        lector.Close()
    End Sub
    Public Sub leerMatricula(ByRef tren As Tren)
        Ag = AgenteBD.getAgente()
        Dim lector As OleDbDataReader = Ag.leer("SELECT * FROM trenes WHERE Matricula='" & tren.Matricula & "';")
        While lector.Read()
            tren.Tipo.TipoTren = (lector(1))
        End While
        lector.Close()
    End Sub
    Public Sub añadir(ByRef tren As Tren)
        Ag.modificar("INSERT INTO trenes VALUES ('" & tren.Matricula & "' , " & tren.Tipo.TipoTren & ");")
    End Sub
    Public Sub modificar(ByRef tren As Tren)
        Ag.modificar("UPDATE trenes SET TipoTren=" & tren.Tipo.TipoTren & " WHERE Matricula='" & tren.Matricula & "';")
    End Sub
    Public Sub eliminar(ByRef tren As Tren)
        Ag.modificar("DELETE FROM trenes WHERE Matricula='" & tren.Matricula & "';")
    End Sub
End Class
