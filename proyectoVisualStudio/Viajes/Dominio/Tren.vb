﻿Public Class Tren
    Private _matricula As String
    Private _tipo As TipoTren
    Private mGestorTren As GestorTren
    Property Matricula As String
        Get
            Return _matricula
        End Get
        Set(ByVal value As String)
            _matricula = value
        End Set
    End Property
    Property Tipo As TipoTren
        Get
            Return _tipo
        End Get
        Set(ByVal value As TipoTren)
            _tipo = value
        End Set
    End Property
    Property Gestor As GestorTren
        Get
            Return mGestorTren
        End Get
        Set(value As GestorTren)

        End Set
    End Property

    Sub New(ByVal matri As String, ByRef tipo As TipoTren)
        Me.Matricula = matri
        Me.Tipo = tipo
        mGestorTren = New GestorTren
    End Sub
    Sub New(ByVal matri As String)
        Me.Matricula = matri
        _tipo = New TipoTren
        mGestorTren = New GestorTren
    End Sub
    Sub New()
        _tipo = New TipoTren
        mGestorTren = New GestorTren
    End Sub
    Public Sub añadir()
        Me.Gestor.añadir(Me)
    End Sub
    Public Sub modificar()
        Me.Gestor.modificar(Me)
    End Sub
    Public Sub eliminar()
        Me.Gestor.eliminar(Me)
    End Sub
    Public Sub leerTodo()
        Me.Gestor.leerTodo()
    End Sub
    Public Sub leerMatricula(ByVal matr As String)
        Me.Matricula = matr
        Me.Gestor.leerMatricula(Me)
    End Sub
End Class
